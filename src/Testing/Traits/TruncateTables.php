<?php

namespace Ata\Cycle\ORM\Testing\Traits;

use Spiral\Database\DatabaseInterface;

trait TruncateTables
{
    public function setupTruncateTables()
    {
        /** @var DatabaseInterface */
        $db = resolve('cycle-db')->getSource(config('cycle.testing.source_class'))->getDatabase();

        $this->beforeApplicationDestroyed(function() use($db){
            foreach ($db->getTables() as $table)
            {
                if ($table->getName() !== config('cycle.migrations.table'))
                {
                    $db->delete($table->getName())->run();
                }
            }
        });
    }
}
