<?php

namespace Ata\Cycle\ORM\Models\Converters;

use Ata\Cycle\ORM\Models\CycleModel;
use Cycle\ORM\Promise\Collection\CollectionPromise;
use Cycle\ORM\Promise\PromiseMany;
use Cycle\ORM\Promise\PromiseOne;
use Cycle\ORM\Promise\Reference;
use Cycle\ORM\Relation\Pivoted\PivotedCollection;
use Cycle\ORM\Relation\Pivoted\PivotedCollectionPromise;
use Doctrine\Common\Collections\ArrayCollection;

class ArrayConverter
{
    /**
     * Array of visited objects; used to prevent cycling.
     *
     * @var array
     */
    protected $visited = [];

    private function extractProperties($object, $deleteNulls = false)
    {
        if ($this->wasVisited($object)) {
            return null;
        }

        if ($object instanceof CycleModel) {
            $this->visited[] = $object;
            return $this->extract($object, $deleteNulls);
        }

        if ($object instanceof Reference
            || $object instanceof PromiseOne) {
            return null;
        }

        if ($object instanceof PivotedCollectionPromise
            || $object instanceof CollectionPromise
            || $object instanceof PromiseMany
        ) {
            return [];
        }

        if ($object instanceof ArrayCollection || $object instanceof PivotedCollection) {
            return $object->map(function ($elem) use ($deleteNulls){
                return $this->extract($elem, $deleteNulls);
            })->toArray();
        }

        if ($object instanceof Arrayable){
            return $object->toArray();
        }

        return $object;
    }

    public function extract(object $object, $deleteNulls = false): array
    {
        $result = [];
        foreach (get_object_vars($object) as $propertyName => $propertyValue) {
            $value = $this->extractProperties($propertyValue, $deleteNulls);

            if (!$deleteNulls){
                $result[$propertyName] = $value;
                continue;
            }

            if (is_array($value) && count($value) == 0){
                continue;
            }

            if ($value !== null) {
                $result[$propertyName] = $value;
            }
        }

        return $result;
    }

    /**
     * Determine if an object has been serialized already.
     *
     * @param mixed $value
     * @return bool
     */
    protected function wasVisited(&$value)
    {
        return in_array($value, $this->visited, true);
    }
}
