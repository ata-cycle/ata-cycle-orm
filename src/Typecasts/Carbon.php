<?php

namespace Ata\Cycle\ORM\Typecasts;

use Carbon\CarbonImmutable;
use Cycle\ORM\Exception\TypecastException;
use Spiral\Database\DatabaseInterface;

class Carbon
{
    public static function create($value):CarbonImmutable
    {
        if (is_string($value)){
            return new CarbonImmutable($value);
        }

        throw_if(!$value instanceof CarbonImmutable, TypecastException::class);

        return $value;
    }

    public static function typecast($value, DatabaseInterface $db): CarbonImmutable
    {
        return new CarbonImmutable(
            $value,
            $db->getDriver()->getTimezone()
        );
    }
}
