<?php


namespace Ata\Cycle\ORM\Models\Traits;


use Cycle\Annotated\Annotation\Column;

trait StringPrimary
{
    /**  @Column(type="string", primary=true) */
    public $id;
}
