<?php

namespace Ata\Cycle\ORM\Tests\Unit;

use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\RelatedModels\HasOneTestModel;
use Ata\Cycle\ORM\Tests\Models\RelatedModels\RelatedModel;
use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;

class ResolveCircularRelationsTest extends BaseDBTestCase
{
    use ArraySubsetAsserts;

    /**
     * @var array
     */
    private $testModelArray;

    protected function getSourceClass()
    {
        return HasOneTestModel::class;
    }

    /**
     * @var array
     */
    public $nestedEntitiesArray;

    /**
     * @var CycleModel
     */
    public $model;

    protected function createEntities(): void
    {
        $this->nestedEntitiesArray = [
            'integer_field' => 1,
            'has_one' =>
                [
                    'integer_field' => 2,
                    'has_many' =>
                        [
                            [
                                'integer_field' => 3,
                                'many_to_many' =>
                                    [
                                        [
                                            'integer_field' => 4
                                        ],
                                        [
                                            'integer_field' => 5
                                        ],
                                    ],
                            ],
                            [
                                'integer_field' => 6
                            ],
                        ]
                ]
        ];

        RelatedModel::create($this->nestedEntitiesArray);
    }

    public function testShouldConvertNestedEntitiesWithoutLeftJoinLoad()
    {
        $one = RelatedModel::load('has_one.has_many.many_to_many')->findOne();
        $object = $one->resolveCircularRelations();

        self::assertNotNull($object->has_one);
        self::assertNull($object->has_one->has_one_inverse);

        self::assertNotEmpty($object->has_one->has_many);

        foreach ($object->has_one->has_many as $hasMany){
            self::assertNull($hasMany->has_many_inverse);
        }

        self::assertNotEmpty($object->has_one->has_many[0]->many_to_many);

        foreach ($object->has_one->has_many[0]->many_to_many as $manyToMany){
            self::assertEmpty($manyToMany->many_to_many_inverse);
        }
    }
}
