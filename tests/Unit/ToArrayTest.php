<?php

namespace Ata\Cycle\ORM\Tests\Unit;

use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\RelatedModels\HasOneTestModel;
use Ata\Cycle\ORM\Tests\Models\RelatedModels\RelatedModel;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Carbon\CarbonImmutable;
use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;

class ToArrayTest extends BaseDBTestCase
{
    use ArraySubsetAsserts;

    /**
     * @var array
     */
    private $testModelArray;

    protected function getSourceClass()
    {
        return HasOneTestModel::class;
    }

    /**
     * @var array
     */
    public $nestedEntitiesArray;

    /**
     * @var CycleModel
     */
    public $model;

    protected function createEntities(): void
    {
        $this->nestedEntitiesArray = [
            'integer_field' => 1,
            'has_one' =>
                [
                    'integer_field' => 2,
                    'has_many' =>
                        [
                            [
                                'integer_field' => 3,
                                'many_to_many' =>
                                    [
                                        [
                                            'integer_field' => 4
                                        ],
                                        [
                                            'integer_field' => 5
                                        ],
                                    ],
                            ],
                            [
                                'integer_field' => 6
                            ],
                        ]
                ]
        ];

        RelatedModel::create($this->nestedEntitiesArray);

        $this->testModelArray = [
            'datetime_field' => new CarbonImmutable("2020-02-02 10:32:15"),
            'date_field' => new CarbonImmutable("2020-02-02"),
            'timestamp_field' => new CarbonImmutable("2020-02-02 10:32:15"),
            'json_field' => [
                '1' => [2, 3, 4]
            ]
        ];
        TestModel::create($this->testModelArray);
    }

    public function testShouldConvertNestedEntitiesWithoutLeftJoinLoad()
    {
        $has_many = $this->nestedEntitiesArray['has_one']['has_many'];
        [$has_many[0], $has_many[1]] = [$has_many[1], $has_many[0]];

        $one = RelatedModel::load('has_one.has_many.many_to_many')->findOne();

        $array = $one->toArray();

        self::assertArraySubset($this->nestedEntitiesArray, $array);
    }

    public function testShouldConvertDateTime()
    {
        $model = TestModel::findOne();

        self::assertEquals($this->testModelArray['datetime_field'], $model->toArray()['datetime_field']);
    }

    public function testShouldConvertDate()
    {
        $model = TestModel::findOne();

        self::assertEquals($this->testModelArray['date_field'], $model->toArray()['date_field']);
    }

    public function testShouldConvertTimestamp()
    {
        $model = TestModel::findOne();

        self::assertEquals($this->testModelArray['timestamp_field'], $model->toArray()['timestamp_field']);
    }

    public function testShouldConvertJsonField()
    {
        $model = TestModel::findOne();

        self::assertEquals($this->testModelArray['json_field'], $model->toArray()['json_field']);
    }
}
