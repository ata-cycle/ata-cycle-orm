<?php

namespace Ata\Cycle\ORM\Tests\Models;

use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Cycle\Annotated\Annotation\Entity;

/**
 * @Entity
*/
class TestPivotModel
{
    use IntPrimary;
}
