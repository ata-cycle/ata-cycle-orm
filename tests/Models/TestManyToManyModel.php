<?php

namespace Ata\Cycle\ORM\Tests\Models;

use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\ManyToMany;

/**
 * @Entity
 *
*/
class TestManyToManyModel extends BaseTestModel
{
    /** @ManyToMany(cascade=true, though=TestPivotModel::class, target=TestRelatedModel::class) */
    public $related;
}
