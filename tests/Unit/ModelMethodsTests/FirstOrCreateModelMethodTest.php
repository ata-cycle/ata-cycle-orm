<?php

namespace Ata\Cycle\ORM\Tests\Unit\ModelMethodsTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Illuminate\Support\Collection;

class FirstOrCreateModelMethodTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public $model1;
    public $model2;
    public $model3;
    public $idsArray;

    protected function createEntities(): void
    {
        $this->model1 = TestModel::create(['integer_field' => 123]);
        $this->model2 = TestModel::create(['integer_field' => 123]);
        $this->model3 = TestModel::create(['integer_field' => 1]);
        $this->idsArray = [
            $this->model3->id,
            $this->model1->id,
            $this->model2->id,
        ];
    }

    /**
     * @throws \Throwable
     */
    public function testFirstOrCreateShouldFindFirstModelTest()
    {
        $this->assertCount(2, TestModel::findAll(['integer_field' => 123]));

        $firstOrCreateToExistingModel = TestModel::where(['integer_field' => 123])->firstOrCreate(
            ['integer_field' => 456]
        );

        $this->assertCount(2, TestModel::findAll(['integer_field' => 123]));
        $this->assertNotInstanceOf(Collection::class, $firstOrCreateToExistingModel);
        $this->assertTrue(in_array($firstOrCreateToExistingModel->id, $this->idsArray));
        $this->assertEquals($this->idsArray[1], $firstOrCreateToExistingModel->id);
        $this->assertEquals(123, $firstOrCreateToExistingModel->integer_field);

    }
// нет теста на обработку ситуации поиска по одному значению поля и записи в это же поле другого значения


    /**
     * @throws \Throwable
     */
    public function testFirstOrCreateShouldCreateModelTest()
    {
        $this->assertCount(0, TestModel::findAll(['integer_field' => 999]));

        $firstOrCreateToNonExistentModel = TestModel::where(['integer_field' => 999])->firstOrCreate(
            ['integer_field' => 999, 'string_field' => '456']
        );

        $this->assertCount(1, TestModel::findAll(['integer_field' => 999]));
        $this->assertInstanceOf(TestModel::class, $firstOrCreateToNonExistentModel);
        $this->assertFalse(in_array($firstOrCreateToNonExistentModel->id, $this->idsArray));
        $this->assertEquals(999, $firstOrCreateToNonExistentModel->integer_field);
        $this->assertEquals('456', $firstOrCreateToNonExistentModel->string_field);
    }

    public function testFirstOrCreateWithTheSameDataTest()
    {
        $this->assertCount(0, TestModel::findAll(['integer_field' => 111]));

        $firstOrCreateToNonExistentModel = TestModel::where(['integer_field' => 111])->firstOrCreate(
            ['integer_field' => 456]
        );

        $this->assertCount(1, TestModel::findAll(['integer_field' => 456]));
        $this->assertEquals(456, $firstOrCreateToNonExistentModel->integer_field);

    }
}
