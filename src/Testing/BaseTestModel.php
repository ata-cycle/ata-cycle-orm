<?php

namespace Ata\Cycle\ORM\Testing;

use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Cycle\Annotated\Annotation\Column;
use Ata\Cycle\ORM\Typecasts\Json;
use Ata\Cycle\ORM\Typecasts\Carbon;

class BaseTestModel extends CycleModel
{
    use IntPrimary;

    /** @Column(type="boolean", nullable=true) */
    public $boolean_field;

    /** @Column(type="integer", nullable=true) */
    public $integer_field;

    /** @Column(type="string", nullable=true) */
    public $string_field;

    /** @Column(type="decimal(2,0)", nullable=true) */
    public $decimal_field;

    /** @Column(type="datetime", nullable=true, typecast=Carbon::class) */
    public $datetime_field;

    /** @Column(type="date", nullable=true, typecast=Carbon::class) */
    public $date_field;

    /** @Column(type="timestamp", nullable=true) */
    public $timestamp_field;

    /** @Column(type="json", nullable=true, typecast=Json::class) */
    public $json_field;
}
