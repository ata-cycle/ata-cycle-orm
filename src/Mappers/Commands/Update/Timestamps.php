<?php

namespace Ata\Cycle\ORM\Mappers\Commands\Update;

use Ata\Cycle\ORM\Mappers\Commands\Interfaces\UpdateMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Carbon\CarbonImmutable;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class Timestamps implements UpdateMapperCommand
{
    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?ContextCarrierInterface $previousCommand): ?ContextCarrierInterface
    {
        $uses = array_flip(class_uses_recursive($entity));

        if (isset($uses[\Ata\Cycle\ORM\Models\Traits\Timestamps::class])) {
            $state->register('updated_at', new CarbonImmutable(), true);
            $previousCommand->registerAppendix('updated_at', new CarbonImmutable());
        }

        return $previousCommand;
    }
}
