<?php

declare(strict_types=1);

namespace Ata\Cycle\ORM\Repositories;

use Ata\Cycle\ORM\Models\CycleModel;
use BadMethodCallException;
use Cycle\ORM\ORMInterface;
use Cycle\ORM\Schema;
use Cycle\ORM\Select;
use Cycle\ORM\Select\ConstrainInterface;
use Cycle\ORM\Select\JoinableLoader;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use ReflectionClass;
use Spiral\Database\Injection\Fragment;
use Spiral\Database\Query\Interpolator;
use Spiral\Database\Query\QueryParameters;
use Spiral\Pagination\Paginator;

/**
 * This repository extends default Select usage and can be used as Select proxy
 *
 *
 * @method AtaRepositoryInterface distinct()
 * @method AtaRepositoryInterface where(...$args);
 * @method AtaRepositoryInterface load(string|array $relation, array $options = []);
 * @method AtaRepositoryInterface andWhere(...$args);
 * @method AtaRepositoryInterface orWhere(...$args);
 * @method AtaRepositoryInterface having(...$args);
 * @method AtaRepositoryInterface andHaving(...$args);
 * @method AtaRepositoryInterface orHaving(...$args);
 * @method AtaRepositoryInterface orderBy($expression, $direction = 'ASC');
 *
 * @method mixed avg($identifier) Perform aggregation (AVG) based on column or expression value.
 * @method mixed min($identifier) Perform aggregation (MIN) based on column or expression value.
 * @method mixed max($identifier) Perform aggregation (MAX) based on column or expression value.
 * @method mixed sum($identifier) Perform aggregation (SUM) based on column or expression value.
 */
class AtaRepository implements AtaRepositoryInterface
{
    /**
     * @var string
     */
    protected $entityClass;

    /** @var Select */
    protected $select;
    /**
     * @var ORMInterface
     */
    protected $orm;
    /**
     * @var string
     */
    protected $role;

    public function select()
    {
        return clone $this->select;
    }

    /**
     * @inheritDoc
     */
    public function findByPK($id): ?object
    {
        return $this->where('id', $id)->findOne();
    }

    /**
     * @inheritDoc
     */
    public function findOne(array $scope = []): ?object
    {
        // FIXME this is another ugly hack
        $query = $this->select->buildQuery();
        $reflection = new ReflectionClass($query);
        $property = $reflection->getProperty('joinTokens');
        $property->setAccessible(true);
        $joinTokens = $property->getValue($query);

        if (count($joinTokens) === 0 ){
            $result = $this->select->fetchOne($scope);

            $this->newSelect();

            return $result;
        }

        // Hack to avoid filtering issues
        $result = $this->findAll($scope);

        $result = isset($result[0]) ? $result[0] : null;

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findAll(array $scope = []): iterable
    {
        $result = collect($this->select->where($scope)->fetchAll());

        $this->newSelect();

        return $result;
    }

    protected function newSelect()
    {
        $this->select = new Select($this->orm, $this->role);
        $this->select->constrain($this->orm->getSource($this->role)->getConstrain());
    }

    public function __clone()
    {
        $this->newSelect();
    }

    public function __construct(Select $select, ORMInterface $orm, string $role)
    {
        $this->entityClass = $orm->getSchema()->define($role, Schema::ENTITY);
        $this->orm = $orm;
        $this->role = $role;

        $this->select = $select;
    }

    /**
     * Copy from SelectQuery __call method
     * @param string $name
     * @param array $arguments
     * @return AtaRepository|Select|mixed
     */
    public function __call(string $name, array $arguments)
    {
        if (in_array(strtoupper($name), ['FETCHALL', 'FETCHONE'])) {
            throw new BadMethodCallException();
        }

        if (in_array(strtoupper($name), ['AVG', 'MIN', 'MAX', 'SUM', 'COUNT'])) {
            // aggregations
            $result = $this->select->__call($name, $arguments);

            $this->newSelect();

            return $result;
        }

        $result = $this->select->{$name}(...$arguments);
        if ($result instanceof Select) {
            return $this;
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findOrFail(array $scope = []): Collection
    {
        $result = $this->findAll($scope);

        throw_if(count($result) == 0, ModelNotFoundException::class);

        return $result;
    }

    public function exists(): bool
    {
        return $this->count() > 0;
    }

    public function notExists(): bool
    {
        return $this->count() == 0;
    }

    /**
     * @inheritDoc
     */
    public function firstOrFail($id = null): object
    {
        $result = $id === null ? $this->findOne() : $this->findOne(['id' => $id]);

        throw_if($result === null, ModelNotFoundException::class);

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function firstOrCreate(array $defaultData = [], array $requiredData = []): object
    {
        $result = $this->findOne($defaultData);
        if ($result === null) {
            $result = ($this->entityClass)::create(array_merge($defaultData, $requiredData));
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function updateOrCreate(array $updatingData = [], array $requiredData = []): Collection
    {
        $result = collect($this->findAll())->each(function (CycleModel $element) use ($updatingData) {
            return $element->update($updatingData);
        });

        if ($result->isEmpty()) {
            $result = collect()->add(($this->entityClass)::create(array_merge($updatingData, $requiredData)));
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function loadLeftJoin($relation, array $options = []): AtaRepositoryInterface
    {
        $leftJoin = ['method' => JoinableLoader::LEFT_JOIN];

        $this->select->load($relation, $options + $leftJoin);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function withoutConstrains(): AtaRepositoryInterface
    {
        $this->select->constrain(null);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function withConstrain(ConstrainInterface $constrain): AtaRepositoryInterface
    {
        $this->select->constrain($constrain);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function whereIf($predicate, ...$arguments): AtaRepositoryInterface
    {
        if ($predicate){
            $this->where(...$arguments);
        }

        return $this;
    }

    public function limit(int $limit): AtaRepositoryInterface
    {
        $paginatedSelect = $this->select();

        $paginatedSelect->limit($limit);

        // very bad solution, need to be replaced when add subquery possibility to cycle orm
        $this->wrapSelect($paginatedSelect);

        return $this;
    }

    public function paginate(int $perPage, int $pageNumber = 0):AtaRepositoryInterface
    {
        $paginatedSelect = $this->select();

        $paginator = (new Paginator($perPage))->withPage($pageNumber);
        $paginator->paginate($paginatedSelect);

        // very bad solution, need to be replaced when add subquery possibility to cycle orm
        $this->wrapSelect($paginatedSelect);

        return $this;
    }

    /**
     * @param Select $paginatedSelect
     */
    protected function wrapSelect(Select $paginatedSelect): void
    {
        $params = new QueryParameters();
        $sqlStatement = $paginatedSelect->buildQuery()->sqlStatement($params);

        $sqlStatement = Interpolator::interpolate($sqlStatement, $params->getParameters());

        $resultStatement = preg_replace('/\sAS\s\"c\d+\"/', '', $sqlStatement);

        $this->select = $this->select()->from(
            (new Fragment('(' . $resultStatement . ') as "' .
                $paginatedSelect->getBuilder()->getLoader()->getAlias() . '"'))
        );
    }
}
