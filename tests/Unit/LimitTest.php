<?php

namespace Ata\Cycle\ORM\Tests\Unit;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;

class LimitTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    protected function createEntities()
    {
        TestModel::create(['integer_field' => 1, 'related' => [
            ['string_field' => '1234']
        ]]);
        TestModel::create(['integer_field' => 2]);
        TestModel::create(['integer_field' => 3]);
        TestModel::create(['integer_field' => 4]);
        TestModel::create(['integer_field' => 5]);
    }

    public function testShouldWorkWithJoins()
    {
        $models = TestModel::orderBy('integer_field')->limit(1)->loadLeftJoin('related')->findAll();

        self::assertEquals(1, $models->count());
    }

    public function testShouldWorkWithoutJoins()
    {
        $model = TestModel::orderBy('integer_field')->findOne();

        self::assertNotNull($model);
    }
}
