<?php

namespace Ata\Cycle\ORM\Tests\Unit;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use BadMethodCallException;
use Jchook\AssertThrows\AssertThrows;

class FetchAllFetchOneTest extends BaseDBTestCase
{
    use AssertThrows;

    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public function testShouldThrowExceptionOnFetchAllMethodCall()
    {
        self::assertThrows(BadMethodCallException::class, function(){
            TestModel::fetchAll();
        });
    }

    public function testShouldThrowExceptionOnFetchOneMethodCall()
    {
        self::assertThrows(BadMethodCallException::class, function(){
            TestModel::fetchOne();
        });
    }
}
