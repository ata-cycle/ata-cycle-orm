<?php

namespace Ata\Cycle\ORM\Tests\Unit\ModelMethodsTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Spiral\Database\Injection\Parameter;

class FindOrFailModelMethodTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public $model1;
    public $model2;
    public $idsArray;

    protected function createEntities(): void
    {
        $this->model1 = TestModel::create(['integer_field' => 123]);
        $this->model2 = TestModel::create(['integer_field' => 123]);
        $this->idsArray = [
            $this->model1->id,
            $this->model2->id,
        ];
    }

    /**
     * @throws \Throwable
     */
    public function testFindOrFailShouldFindModelsTest()
    {
        $this->assertCount(2, TestModel::findOrFail(['integer_field' => 123]));

        TestModel::where(['integer_field' => 123])->findOrFail()->each(function ($element) {
            $this->assertTrue(in_array($element->id, $this->idsArray));
        });
    }

    /**
     * @throws \Throwable
     */
    public function testFindOrFailShouldGiveExceptionTest()
    {
        $deletedModel1Id = $this->model1->id;
        $deletedModel2Id = $this->model2->id;

        TestModel::where('id', 'in', new Parameter([$deletedModel1Id, $deletedModel2Id]))->findAll()->each(function($model){
            $model->delete();
        });

        $this->assertThrows(ModelNotFoundException::class, function() use ($deletedModel1Id)
        {
            TestModel::where(['id' => $deletedModel1Id])->findOrFail();
        });

        $this->assertThrows(ModelNotFoundException::class, function() use ($deletedModel2Id)
        {
            TestModel::where(['id' => $deletedModel2Id])->findOrFail();
        });
    }
}
