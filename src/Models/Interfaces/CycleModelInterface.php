<?php

namespace Ata\Cycle\ORM\Models\Interfaces;

use Cycle\ORM\Select\ConstrainInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface CycleModelInterface
{
    /**
     * Возвращает сущность по переданному идентификатору
     *
     * MyModel::findByPK(123)
     *
     * @param $id
     *
     * @return static
     */
    static function findByPK($id);

    /**
     * Возвращает сущность по переданному запросу.
     * Может использоваться в цепочке запросов.
     *
     * Возврат первой модели в БД.
     * MyModel::findOne()
     *
     * Возврат первой модели, удовлетворяющей условиям
     * MyModel::findOne(['field'=>'value'])
     *
     * Возврат первой модели, удовлетворяющей условиям цепочки.
     * MyModel::where('field', 'value')->findOne()
     *
     * Возврат первой модели, удовлетворяющей условиям цепочки, с добавлением дополнительных условий
     * MyModel::where('field1', 'value')->findOne(['field2' => 'value'])
     *
     * @param array $scope
     *
     * @return static
     */
    static function findOne(array $scope = []);

    /**
     * Возвращает коллекцию сущностей по переданному запросу.
     * Может использовать в цепочке запросов.
     *
     * Возврат всех сущностей в БД
     * MyModel::findAll()
     *
     * Возврат всех сущностей в БД, удовлетворяющих условиям
     * MyModel::findAll(['field'=>'value'])
     *
     * Возврат всех сущностей в БД, удовлетворяющих условиям цепочки.
     * MyModel::where('field', 'value')->findAll()
     *
     * Возврат всех сущностей в БД, удовлетворяющих условиям цепочки, с добавлением дополнительных условий
     * MyModel::where('field1', 'value')->findAll(['field2' => 'value'])
     *
     * @param array $scope
     *
     * @return Collection
     */
    static function findAll(array $scope = []): iterable;

    /**
     * Возвращает коллекцию сущностей по переданному запросу.
     * Если не нашел ни одну - выбрасывает исключение.
     * Используется аналогично findAll
     *
     * Возврат всех сущностей в БД
     * MyModel::findOrFail()
     *
     * Возврат всех сущностей в БД, удовлетворяющих условиям
     * MyModel::findOrFail(['field'=>'value'])
     *
     * Возврат всех сущностей в БД, удовлетворяющих условиям цепочки.
     * MyModel::where('field', 'value')->findOrFail()
     *
     * Возврат всех сущностей в БД, удовлетворяющих условиям цепочки, с добавлением дополнительных условий
     * MyModel::where('field1', 'value')->findOrFail(['field2' => 'value'])
     *
     * @param array $scope
     *
     * @return Collection
     */
    static function findOrFail(array $scope = []): Collection;

    /**
     * Возвращает сущность по переданному запросу или выбрасывает исключение
     * Может использоваться в цепочке запросов.
     * Используется аналогично findOne
     *
     * Возврат первой модели в БД.
     * MyModel::firstOrFail()
     *
     * Возврат первой модели, удовлетворяющей условиям
     * MyModel::firstOrFail(['field'=>'value'])
     *
     * Возврат первой модели, удовлетворяющей условиям цепочки.
     * MyModel::where('field', 'value')->firstOrFail()
     *
     * Возврат первой модели, удовлетворяющей условиям цепочки, с добавлением дополнительных условий
     * MyModel::where('field1', 'value')->firstOrFail(['field2' => 'value'])
     *
     * @param null $id
     *
     * @return static
     */
    static function firstOrFail($id = null);

    /**
     * Возвращает сущность по переданному запросу, если ее нет, создает новую и возвращает ее.
     * Используется аналогично findOne
     *
     * Возврат первой модели в БД или ее создание, если ничего не создано
     * MyModel::firstOrCreate(['field'=>'value'])
     *
     * Возврат первой модели, удовлетворяющей условиям цепочки,  или ее создание.
     * MyModel::where('field1', 'value1')->firstOrFail(['field2'=>'value2'])
     *
     * Возврат первой модели, удовлетворяющей условиям цепочки, или ее создание с дополнительными данными,
     * если этой сущности не существует
     * MyModel::where('field1', 'value1')->firstOrFail(['field2'=>'value2'], ['field3' => 'value3'])
     *
     * @param array $requiredData Ищет по этому массиву сущность, если не находит, создает ее с этими полями
     * @param array $creatingData Дополнительные данные для создания сущности
     *
     * @return static
     */
    static function firstOrCreate(array $requiredData = [], array $creatingData = []);

    /**
     * Обновляет сущности, удовлетворяющие переданной цепочке запросов
     * Если не существует сущностей, удовлетворяющих цепочке, тогда создает одну сущность с дополнительными полями
     *
     * Обновление всех сущностей в таблице
     * MyModel::updateOrCreate(['field' => 'value'])
     *
     * Обновление всех сущностей в таблице. Добавляет доп.поля, если в таблице изначально нет ни одной записи
     * MyModel::updateOrCreate(['field1' => 'value1'], ['field2' => 'value2'])
     *
     * Обновление всех сущностей в таблице, удовлетворяющих условиям цепочки
     * MyModel::where('field', 'value')->updateOrCreate(['field1' => 'value1'])
     *
     * @param array $updatingData
     * @param array $requiredData
     *
     * @return Collection
     */
    static function updateOrCreate(array $updatingData = [], array $requiredData = []): Collection;

    /**
     * Убирает области видимости из запроса, если они существовали до этого.
     * Используется в цепочке запросов
     *
     * @return static
     */
    static function withoutConstrains();

    /**
     * Перезаписывает область видимости в запросе
     * Используется в цепочке запросов
     *
     * @param ConstrainInterface $constrain
     *
     * @return static
     */
    static function withConstrain(ConstrainInterface $constrain);

    /**
     * Помечает запрос для возврата только уникальных значений
     *
     * @return static
     */
    static function distinct();

    /**
     * Добавляет дополнительное условие для запроса в БД в секцию WHERE
     *
     * @param array $args
     *
     * @return static
     */
    static function where(...$args);

    /**
     * Добавляет дополнительное условие для запроса в БД, если выполняется предикат, написанный в PHP-коде
     *
     * $predicate = $model->field === 'asdf';
     * MyModel::whereIf($predicate, ['field1', 'value1'])
     *
     * @param bool $predicate
     * @param array $args
     *
     * @return static
     */
    static function whereIf(bool $predicate, ...$args);

    /**
     * Загружает отношения. Примеры использования можно найти на https://cycle-orm.dev/docs/query-builder-relations#loading-relations
     *
     * @param $relation
     * @param array $options
     *
     * @return static
     */
    static function load($relation, array $options = []);

    /**
     * Используется аналогично методу load, только по умолчанию сущности загружаются через LEFT_JOIN, а не lazy-запросом
     *
     * @param $relation
     * @param array $options
     * @return static
     */
    static function loadLeftJoin($relation, array $options = []);

    /**
     *
     * Добавляет условия в секцию запроса WHERE с использованием оператора AND
     *
     * @param array $args
     *
     * @return static
     */
    static function andWhere(...$args);

    /**
     * Добавляет условия в секцию запроса WHERE с использованием оператора OR
     *
     * @param array $args
     *
     * @return static
     */
    static function orWhere(...$args);

    /**
     * Добавляет дополнительное условие для запроса в БД в секцию HAVING
     *
     * @param array $args
     *
     * @return static
     */
    static function having(...$args);

    /**
     * Добавляет дополнительное условие в секцию запроса HAVING с использованием оператора AND
     *
     * @param array $args
     *
     * @return static
     */
    static function andHaving(...$args);

    /**
     * Добавляет дополнительное условие в секцию запроса HAVING с использованием оператора OR
     *
     * @param array $args
     *
     * @return static
     */
    static function orHaving(...$args);

    /**
     * Добавляет условие сортировки.
     *
     * @param $expression
     * @param string $direction Способ сортировки. Может принимать значения ASC и DESC
     * @return static
     */
    static function orderBy($expression, $direction = 'ASC');

    /**
     * Ограничивает выборку по переданному значению
     * Возвращает первые $limit элементов
     *
     * @param int $limit
     *
     * @return static
     */
    static function limit(int $limit);

    /**
     * Пропускает первые $offset элементов, возвращая все элементы после
     *
     * @param int $offset
     *
     * @return static
     */
    static function offset(int $offset);

    /**
     * Осуществляет пагинацию на основе переданных параметров
     *
     * Возвращает первые 10 сущностей
     * MyModel::paginate(10)
     *
     * Возвращает сущности с 11 по 20
     * MyModel::paginate(10, 1)
     *
     * @param int $perPage
     * @param int $pageNumber
     *
     * @return static
     */
    static function paginate(int $perPage, int $pageNumber = 0);

    /**
     * Возвращает true, если существует сущности, удовлетворяющие цепочке запроса.
     *
     * Вернет true, если в таблице есть хоть одна запись
     * MyModel::exists()
     *
     * Вернет true, если существует хотя бы одна запись, удовлетворяющая запросу
     * MyModel::where('field', 'value')->exists()
    */
    static function exists(): bool;

    /**
     * Возвращает true, если не существует ни одной сущности, удовлетворяющей запросу
     *
     * Вернет true, если в таблице нет ни одной записи
     * MyModel::notExists()
     *
     * Вернет true, если в таблице не ни одной записи, удовлетворяющей запросу
     * MyModel::where('field', 'value')->notExists()
    */
    static function notExists(): bool;

    /**
     * Возвращает среднее значение для переданного выражения
     * MyModel::avg('field')
     *
     * @param $identifier
     *
     * @return mixed
     */
    static function avg($identifier);

    /**
     * Возвращает минимальное значение для переданного выражения
     * MyModel::min('field')
     *
     * @param $identifier
     *
     * @return mixed
     */
    static function min($identifier);

    /**
     * Возвращает максимальное значение для переданного выражения
     * MyModel::max('field')
     *
     * @param $identifier
     *
     * @return mixed
     */
    static function max($identifier);

    /**
     * Возвращает суммарное значение для переданного выражения
     * MyModel::sum('field')
     *
     * @param $identifier
     *
     * @return mixed
     */
    static function sum($identifier);

    /**
     * Заполняет модель входящими данными
     * $model->fill(['field'=>'value'])
     *
     * @param array $data
     *
     * @return static
     */
    public function fill(array $data);

    /**
     * Превращает модель в ассоциативный массив рекурсивно.
     * По умолчанию убирает поля без значений.
     *
     * $model->toArray()
     *
     * @param bool $deleteNulls
     *
     * @return array
     */
    function toArray($deleteNulls = true): array;

    /**
     * Сохраняет модель в БД
     *
     * @param bool $cascade
     *
     * @return static
     */
    function save(bool $cascade = true);


    /**
     * Удаляет модель из БД
     */
    function delete(): void;

    /**
     * Создает модель с переданными данными и сохраняет ее в БД
     * MyModel::create(['field' => 'value'])
     *
     * @param array $data
     *
     * @return static
     */
    static function create(array $data = []);

    /**
     * Создает модель с переданными данными, но НЕ сохраняет ее в БД
     * MyModel::make(['field' => 'value'])
     *
     * @param array $data
     *
     * @return static
     */
    static function make(array $data = []);

    /**
     * Создает ларавельскую коллекцию из переданного массива моделей
     *
     * @param array $models
     *
     * @return Collection
     */
    static function newCollection(array $models = []): Collection;

    /**
     * Обновляет сущность переданными данными, записывая изменения в БД
     * $model->update(['field' => 'value'])
     *
     * @param array $data
     *
     * @return static
     */
    function update(array $data = []);

    /**
     * Обновляет карту отношений.
     *
     * @param array $relations
     */
    function updateRelations(array $relations): void;
}
