<?php

namespace Ata\Cycle\ORM\Tests\Unit;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;

class AggregatesTest extends BaseDBTestCase
{

    protected function getSourceClass()
    {
        return TestModel::class;
    }

    protected function createEntities()
    {
        TestModel::create(['integer_field' => 1]);
        TestModel::create(['integer_field' => 2]);
        TestModel::create(['integer_field' => 3]);
        TestModel::create(['integer_field' => 4]);
    }

    public function testShouldCorrectReturnEntitiesAfterAggregatesCall()
    {
        self::assertEquals(1, TestModel::where('integer_field', 1)->count());
        self::assertEquals(4, TestModel::findAll()->count());
    }
}
