<?php

namespace Ata\Cycle\ORM\Drivers;

use Ata\Cycle\ORM\Drivers\Handlers\PostgresHandler;
use Spiral\Database\Driver\Driver;
use Spiral\Database\Driver\Postgres\PostgresCompiler;
use Spiral\Database\Driver\Postgres\Query\PostgresInsertQuery;
use Spiral\Database\Query\DeleteQuery;
use Spiral\Database\Query\QueryBuilder;
use Spiral\Database\Query\SelectQuery;
use Spiral\Database\Query\UpdateQuery;

class PostgresDriver extends \Spiral\Database\Driver\Postgres\PostgresDriver
{
    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        // default query builder
        Driver::__construct(
            $options,
            new PostgresHandler(),
            new PostgresCompiler('""'),
            new QueryBuilder(
                new SelectQuery(),
                new PostgresInsertQuery(),
                new UpdateQuery(),
                new DeleteQuery()
            )
        );
    }
}
