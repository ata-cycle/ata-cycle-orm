<?php

namespace Ata\Cycle\ORM\Tests\Models;

use Ata\Cycle\ORM\Models\Traits\SoftDeletes;
use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Ata\Cycle\ORM\Constrains\NotDeletedConstrain;

/**
 * @Entity(constrain=NotDeletedConstrain::class)
*/
class TestSoftDeleteModel extends BaseTestModel
{
    use SoftDeletes;
}
