<?php


namespace Ata\Cycle\ORM\Models;

use Ata\Cycle\ORM\Exceptions\NotImplementedRelationException;
use Ata\Cycle\ORM\Models\Converters\ArrayConverter;
use Ata\Cycle\ORM\Models\Converters\CircularRelationsConverter;
use Ata\Cycle\ORM\Models\Interfaces\CycleModelInterface;
use Ata\Cycle\ORM\Models\Traits\ModelEvents;
use Ata\Cycle\ORM\Models\Traits\RepositoryMethods;
use Ata\Cycle\ORM\Repositories\AtaRepositoryInterface;
use Cycle\ORM\Exception\TypecastException;
use Cycle\ORM\Promise\Reference;
use Cycle\ORM\Relation;
use Cycle\ORM\SchemaInterface;
use Cycle\ORM\Select\ConstrainInterface;
use Cycle\ORM\Select\SourceInterface;
use Cycle\ORM\Transaction;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Spiral\Database\Injection\Parameter;

/**
 * @method static null|static findByPk(mixed $id)
 * @method static null|static findOne(array $scope = [])
 * @method static iterable findAll(array $scope = [])
 *
 * @method static Collection findOrFail(array $scope = [])
 * @method static static firstOrFail($id = null)
 * @method static static firstOrCreate(array $requiredData = [], array $creatingData = [])
 * @method static Collection updateOrCreate(array $updatingData = [], array $requiredData = [])
 *
 * @method static static withoutConstrains()
 * @method static static withConstrain(ConstrainInterface $constrain)
 * @method static static distinct()
 * @method static static where(...$args);
 * @method static static whereIf(bool $predicate, ...$args);
 * @method static static load(string|array $relation, array $options = []);
 * @method static static loadLeftJoin(string|array $relation, array $options = []);
 * @method static static andWhere(...$args);
 * @method static static orWhere(...$args);
 * @method static static having(...$args);
 * @method static static andHaving(...$args);
 * @method static static orHaving(...$args);
 * @method static static orderBy($expression, $direction = 'ASC');
 * @method static static limit(int $limit);
 * @method static static offset(int $offset);
 * @method static bool exists();
 * @method static bool notExists();
 *
 * @method static mixed avg($identifier) Perform aggregation (AVG) based on column or expression value.
 * @method static mixed min($identifier) Perform aggregation (MIN) based on column or expression value.
 * @method static mixed max($identifier) Perform aggregation (MAX) based on column or expression value.
 * @method static mixed sum($identifier) Perform aggregation (SUM) based on column or expression value.
 */
abstract class CycleModel implements CycleModelInterface
{
    use RepositoryMethods;
    use ModelEvents;

    public static function orm(): AtaRepositoryInterface
    {
        return resolve('cycle-db')->getRepository(static::class);
    }

    public function __construct(array $data = [])
    {
        /** @var $schema SchemaInterface */
        $schema = resolve('cycle-db')->getSchema();
        $relations = $schema->getRelations(static::class);

        foreach ($relations as $name){
            $relation = $schema->defineRelation(static::class, $name);
            switch ($relation[Relation::TYPE]) {
                case Relation::MANY_TO_MANY:
                    $this->{$name} = new Relation\Pivoted\PivotedCollection();
                    break;
                case Relation::HAS_MANY:
                case Relation::MORPHED_HAS_MANY:
                    $this->{$name} = new ArrayCollection();
                    break;
            }
        }

        $this->fill($data);
    }

    /**
     * @param array $data
     * @return static
     */
    public function fill(array $data)
    {
        foreach ($data as $key => $value) {
            $this->__set($key, $value);
        }
        return $this;
    }

    public function toArray($deleteNulls = true): array
    {
        return (new ArrayConverter())->extract($this, $deleteNulls);
    }

    /**
     * @return static|object
     */
    public function resolveCircularRelations()
    {
        return (new CircularRelationsConverter())->extract($this);
    }

    public function __get($name)
    {
        if (method_exists($this, Str::camel('get_' . $name . '_attribute'))) {
            return $this->{(Str::camel('get_' . $name . '_attribute'))}();
        }
        return $this->{$name};
    }

    public function __set($name, $value)
    {
        if (method_exists($this, Str::camel('set_' . $name . '_attribute'))) {
            return $this->{(Str::camel('set_' . $name . '_attribute'))}($value);
        }

        // Проверяем на вложенность, необходимо получить тип вложенных сущностей, чтобы корректно инстанцировать
        /** @var $schema SchemaInterface */
        $schema = resolve('cycle-db')->getSchema();
        $relations = $schema->getRelations(static::class);

        if (in_array($name, $relations)) {
            $relation = $schema->defineRelation(static::class, $name);
            if ($relation[Relation::TYPE] === Relation::BELONGS_TO_MORPHED)
            {
                return $this->{$name} = $this->createEntity($value, $relation[Relation::TARGET]);
            }

            $innerClass = $schema->define($relation[Relation::TARGET], $schema::ENTITY);

            // заполняем в зависимости от того, что внутри
            switch ($relation[Relation::TYPE]) {
                case Relation::BELONGS_TO:
                case Relation::HAS_ONE:
                case Relation::REFERS_TO:
                    return $this->{$name} = $this->createEntity($value, $innerClass);
                case Relation::MANY_TO_MANY:
                case Relation::HAS_MANY:
                case Relation::MORPHED_HAS_MANY:
                    $this->{$name}->clear();

                    foreach ($value as $entity) {
                        $entity = $this->createEntity($entity, $innerClass);

                        $this->{$name}->add($entity);
                    }
                    return $this->{$name};
            }
        }

        $typeCasts = $schema->define(static::class, SchemaInterface::TYPECAST);

        if (array_key_exists($name, $typeCasts) && is_array($typeCasts[$name])) {
            return $this->{$name} = ($typeCasts[$name][0])::create($value);
        }

        // Поведение по умолчанию
        return $this->{$name} = $value;
    }

    /**
     * Save the model to the database.
     *
     * @param bool $cascade
     * @return static
     */
    public function save(bool $cascade = true)
    {
        $transaction = resolve('cycle-db.transaction');
        $transaction->persist($this, $cascade ? Transaction::MODE_CASCADE : Transaction::MODE_ENTITY_ONLY);
        $transaction->run();

        return $this;
    }

    /**
     * Delete the model from the database.
     */
    public function delete(): void
    {
        $transaction = resolve('cycle-db.transaction');
        $transaction->delete($this);
        $transaction->run();
    }

    /**
     * @param array $data
     * @return static
     */
    public static function create(array $data = [])
    {
        return (new static($data))->save();
    }

    /**
     * @param array $data
     * @return static
     */
    public static function make(array $data = [])
    {
        return (new static($data));
    }

    public static function newCollection(array $models = []): Collection
    {
        return collect($models);
    }

    /**
     * @param array $data
     * @return static
     */
    public function update(array $data = [])
    {
        return $this->fill($data)->save();
    }

    /**
     * Обновляет карту отношений.
     * @param array $relations
     */
    public function updateRelations(array $relations): void
    {
        /** @var $schema SchemaInterface */
        $schema = resolve('cycle-db')->getSchema();

        foreach ($relations as $name => $ids) {
            $relation = $schema->defineRelation(static::class, $name);
            $nullable = $relation[Relation::SCHEMA][Relation::NULLABLE];

            if ($relation[Relation::TYPE] == Relation::HAS_MANY) {
                $innerClass = $schema->define($relation[Relation::TARGET], $schema::ENTITY);
            } elseif ($relation[Relation::TYPE] == Relation::MANY_TO_MANY) {
                $innerClass = $schema->define($relation[Relation::SCHEMA][Relation::THROUGH_ENTITY], $schema::ENTITY);
            }

            /** @var $source SourceInterface */
            $source = resolve('cycle-db')->getSource($innerClass);
            $table = $source->getDatabase()
                ->table($source->getTable());

            switch ($relation[Relation::TYPE]) {
                case Relation::MANY_TO_MANY:
                    $existingIds = collect(
                        $table->select($relation[Relation::SCHEMA][Relation::THROUGH_OUTER_KEY])
                            ->where($relation[Relation::SCHEMA][Relation::THROUGH_INNER_KEY], $this->id)
                            ->fetchAll())
                        ->map(function ($row) use ($relation) {
                            return $row[$relation[Relation::SCHEMA][Relation::THROUGH_OUTER_KEY]];
                        });
                    $diff = collect($ids)->diff($existingIds)->map(function ($elem) use ($relation) {
                        return [
                            $relation[Relation::SCHEMA][Relation::THROUGH_INNER_KEY] => $this->id,
                            $relation[Relation::SCHEMA][Relation::THROUGH_OUTER_KEY] => $elem,
                        ];
                    });
                    if (!$diff->isEmpty()) {
                        $table->insert()
                            ->columns(
                                $relation[Relation::SCHEMA][Relation::THROUGH_INNER_KEY],
                                $relation[Relation::SCHEMA][Relation::THROUGH_OUTER_KEY]
                            )->values($diff->toArray())->run();
                    }

                    $table->delete()
                        ->where($relation[Relation::SCHEMA][Relation::THROUGH_INNER_KEY], $this->id)
                        ->andWhere($relation[Relation::SCHEMA][Relation::THROUGH_OUTER_KEY], 'not in', new Parameter($ids))->run();

                    break;
                case Relation::HAS_MANY:
                    $existingIds = collect(
                        $table->select('id')
                            ->where($relation[Relation::SCHEMA][Relation::OUTER_KEY], $this->id)
                            ->fetchAll())
                        ->map(function ($row) {
                            return $row['id'];
                        });

                    $diff = collect($ids)->diff($existingIds);

                    if (!$diff->isEmpty()) {
                        $table
                            ->update([$relation[Relation::SCHEMA][Relation::OUTER_KEY] => $this->id])
                            ->where('id', 'in', new Parameter($diff->toArray()))->run();
                    }

                    $removeEntities = $nullable
                        ? $table->update([$relation[Relation::SCHEMA][Relation::OUTER_KEY] => null])
                        : $table->delete();

                    $removeEntities
                        ->where($relation[Relation::SCHEMA][Relation::OUTER_KEY], $this->id)
                        ->andWhere('id', 'not in', new Parameter($ids))
                        ->run();

                    break;
                default:
                    throw new NotImplementedRelationException(static::class, $name, $relation[Relation::TYPE]);
            }
        }
    }

    protected function createEntity($object, $class)
    {
        if ($object instanceof Reference){
            return $object;
        }

        if (is_array($object)) {
            return new $class($object);
        } elseif (!$object instanceof $class) {
            throw new TypecastException();
        }
        return $object;
    }

    public static function __callStatic($name, $arguments)
    {
        return static::orm()->{$name}(...$arguments);
    }
}
