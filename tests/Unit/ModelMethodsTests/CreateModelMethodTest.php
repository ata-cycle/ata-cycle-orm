<?php

namespace Ata\Cycle\ORM\Tests\Unit\ModelMethodsTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;

class CreateModelMethodTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public function testCreateModelWithoutDataTest()
    {
        $modelWithoutData = TestModel::create();

        $this->assertInstanceOf(TestModel::class, $modelWithoutData);
        $this->assertNotNull($modelWithoutData->id);

        resolve('cycle-db.heap-clean');

        self::assertEquals($modelWithoutData->id, TestModel::findOne()->id);
    }

    public function testCreateModelWithDataTest()
    {
        $modelWithData = TestModel::create(['integer_field' => 123]);

        $this->assertInstanceOf(TestModel::class, $modelWithData);
        $this->assertNotNull($modelWithData->id);
        $this->assertEquals(123, $modelWithData->integer_field);

        resolve('cycle-db.heap-clean');

        self::assertEquals($modelWithData->id, TestModel::findOne()->id);
        self::assertEquals(123, TestModel::findOne()->integer_field);
    }
}
