<?php

namespace Ata\Cycle\ORM\Tests\Unit\ModelMethodsTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Ata\Cycle\ORM\Tests\Models\TestRelatedModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FirstOrFailModelMethodTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public $firstModel;
    public $secondModel;
    public $idsArray;

    protected function createEntities(): void
    {
        $this->firstModel = TestModel::create();
        $this->secondModel = TestModel::create(['integer_field' => 123]);
        $this->idsArray = [
            $this->firstModel->id,
            $this->secondModel->id,
        ];
    }

    /**
     * @throws \Throwable
     */
    public function testFirstOrFailShouldFindFirstModelTest()
    {
        $this->assertTrue(in_array(TestModel::firstOrFail()->id, $this->idsArray));
        $this->assertEquals($this->idsArray[0], TestModel::firstOrFail()->id);
    }

    /**
     * @throws \Throwable
     */
    public function testFirstOrFailShouldGiveExceptionTest()
    {
        $this->assertThrows(ModelNotFoundException::class, function()
        {
            TestRelatedModel::firstOrFail();
        });
    }
}
