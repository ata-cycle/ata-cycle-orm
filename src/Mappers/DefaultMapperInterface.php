<?php


namespace Ata\Cycle\ORM\Mappers;


use Cycle\ORM\Command\CommandInterface;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;
use Cycle\ORM\Select\SourceInterface;

/**
 * This interface need for compatibility with mapper implementation
 */
interface DefaultMapperInterface
{
    function defaultCreate($entity, Node $node, State $state): ContextCarrierInterface;

    function defaultUpdate($entity, Node $node, State $state): ContextCarrierInterface;

    function defaultDelete($entity, Node $node, State $state): CommandInterface;

    function fetchEntityFields($entity): array;

    function getPrimaryKey(): string;

    function getPrimaryColumn(): string;

    function getSource(): SourceInterface;
}
