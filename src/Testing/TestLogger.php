<?php

namespace Ata\Cycle\ORM\Testing;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;
use Psr\Log\LogLevel;

class TestLogger implements LoggerInterface
{
    use LoggerTrait;

    private $display;

    private $countWrites;
    private $countReads;

    public function __construct()
    {
        $this->countWrites = 0;
        $this->countReads = 0;
    }

    public function log($level, $message, array $context = []): void
    {
        if (!empty($context['query'])) {
            $sql = strtolower($context['query']);
            if (
                strpos($sql, 'insert') === 0
                || strpos($sql, 'update') === 0
                || strpos($sql, 'delete') === 0
            ) {
                $this->countWrites++;
            } elseif (!$this->isPostgresSystemQuery($sql)) {
                $this->countReads++;
            }
        }

        if (!$this->display) {
            return;
        }

        if ($level === LogLevel::ERROR) {
            dump(" \n! \033[31m" . $message . "\033[0m");
        } elseif ($level === LogLevel::ALERT) {
            dump(" \n! \033[35m" . $message . "\033[0m");
        } elseif (strpos($message, 'SHOW') === 0) {
            dump(" \n> \033[34m" . $message . "\033[0m");
        } else {
            if ($this->isPostgresSystemQuery($message)) {
                dump(" \n> \033[90m" . $message . "\033[0m");

                return;
            }

            if (strpos($message, 'SELECT') === 0) {
                dump(" \n> \033[32m" . $message . "\033[0m");
            } elseif (strpos($message, 'INSERT') === 0) {
                dump(" \n> \033[36m" . $message . "\033[0m");
            } else {
                dump(" \n> \033[33m" . $message . "\033[0m");
            }
        }
    }

    public function display(): void
    {
        $this->display = true;
    }

    public function hide(): void
    {
        $this->display = false;
    }

    protected function isPostgresSystemQuery(string $query): bool
    {
        $query = strtolower($query);

        return
            strpos($query, 'tc.constraint_name')
            || strpos($query, 'pg_indexes')
            || strpos($query, 'tc.constraint_name')
            || strpos($query, 'pg_constraint')
            || strpos($query, 'information_schema')
            || strpos($query, 'pg_class');
    }
}
