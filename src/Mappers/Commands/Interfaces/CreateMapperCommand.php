<?php


namespace Ata\Cycle\ORM\Mappers\Commands\Interfaces;


use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

interface CreateMapperCommand
{
    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?ContextCarrierInterface $previousCommand): ?ContextCarrierInterface;
}
