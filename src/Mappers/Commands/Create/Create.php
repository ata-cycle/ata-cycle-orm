<?php


namespace Ata\Cycle\ORM\Mappers\Commands\Create;


use Ata\Cycle\ORM\Mappers\Commands\Interfaces\CreateMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class Create implements CreateMapperCommand
{

    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?ContextCarrierInterface $previousCommand): ?ContextCarrierInterface
    {
        return $mapper->defaultCreate($entity, $node, $state);
    }
}
