<?php


namespace Ata\Cycle\ORM\Validation;


use Illuminate\Contracts\Validation\Rule;

class Unique implements Rule
{
    /**
     * @var string
     */
    private $class;

    public function __construct(string $class)
    {
        $this->class = $class;
    }

    /**
     * @inheritDoc
     */
    public function passes($attribute, $value)
    {
        return ($this->class)::where($attribute, $value)->notExists();
    }

    /**
     * @inheritDoc
     */
    public function message()
    {
        return 'The :attribute with this value already exists.';
    }
}
