<?php

namespace Ata\Cycle\ORM\Models\Converters;

interface Arrayable
{
    /**
     * @return mixed
     */
    function toArray();
}
