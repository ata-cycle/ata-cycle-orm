<?php

namespace Ata\Cycle\ORM\Testing\Traits;

use Spiral\Database\DatabaseInterface;

trait DatabaseTransactions
{
    public function setupDatabaseTransactions()
    {
        /** @var $db DatabaseInterface */
        $db = resolve('cycle-db')->getSource(config('cycle.testing.source_class'))->getDatabase();

        $db->begin();

        $this->beforeApplicationDestroyed(function() use($db){
            $db->rollback();
        });
    }
}
