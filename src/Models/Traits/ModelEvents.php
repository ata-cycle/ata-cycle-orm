<?php

namespace Ata\Cycle\ORM\Models\Traits;

use Ata\Cycle\ORM\Mappers\Commands\Interfaces\CreateMapperCommand;
use Ata\Cycle\ORM\Mappers\Commands\Interfaces\DeleteMapperCommand;
use Ata\Cycle\ORM\Mappers\Commands\Interfaces\UpdateMapperCommand;

trait ModelEvents
{
    /**
     * @return array<CreateMapperCommand>
     */
    public static function beforeCreate(): array
    {
        return [];
    }

    /**
     * @return array<CreateMapperCommand>
     */
    public static function afterCreate(): array
    {
        return [];
    }

    /**
     * @return array<UpdateMapperCommand>
     */
    public static function beforeUpdate(): array
    {
        return [];
    }

    /**
     * @return array<UpdateMapperCommand>
     */
    public static function afterUpdate(): array
    {
        return [];
    }

    /**
     * @return array<DeleteMapperCommand>
     */
    public static function beforeDelete(): array
    {
        return [];
    }

    /**
     * @return array<DeleteMapperCommand>
     */
    public static function afterDelete(): array
    {
        return [];
    }
}
