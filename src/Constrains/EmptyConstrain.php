<?php

namespace Ata\Cycle\ORM\Constrains;

use Cycle\ORM\Select\ConstrainInterface;
use Cycle\ORM\Select\QueryBuilder;

class EmptyConstrain implements ConstrainInterface
{
    public function apply(QueryBuilder $query)
    {
        // this function is need to be empty
    }
}

