<?php

namespace Ata\Cycle\ORM\Validation;

use Illuminate\Contracts\Validation\Rule;

class Exists implements Rule
{
    /**
     * @var string
     */
    private $property;
    /**
     * @var string
     */
    private $class;

    public function __construct(string $class, string $property)
    {
        $this->class = $class;
        $this->property = $property;
    }

    /**
     * @inheritDoc
     */
    public function passes($attribute, $value)
    {
        return ($this->class)::where($this->property, $value)->exists();
    }

    /**
     * @inheritDoc
     */
    public function message()
    {
        return 'The :attribute not exists in database.';
    }
}
