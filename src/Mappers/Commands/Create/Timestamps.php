<?php


namespace Ata\Cycle\ORM\Mappers\Commands\Create;


use Ata\Cycle\ORM\Mappers\Commands\Interfaces\CreateMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Ata\Cycle\ORM\Models\Traits\ModelEvents;
use Carbon\CarbonImmutable;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class Timestamps implements CreateMapperCommand
{

    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?ContextCarrierInterface $previousCommand): ?ContextCarrierInterface
    {
        $uses = array_flip(class_uses_recursive($entity));

        if (isset($uses[\Ata\Cycle\ORM\Models\Traits\Timestamps::class])) {
            $state->register('created_at', new CarbonImmutable(), true);
            $previousCommand->register('created_at', new CarbonImmutable(), true);

            $state->register('updated_at', new CarbonImmutable(), true);
            $previousCommand->register('updated_at', new CarbonImmutable(), true);
        }

        return $previousCommand;
    }
}
