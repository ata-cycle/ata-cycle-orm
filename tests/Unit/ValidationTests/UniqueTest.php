<?php

namespace Ata\Cycle\ORM\Tests\Unit\ValidationTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Ata\Cycle\ORM\Validation\Exists;
use Ata\Cycle\ORM\Validation\Unique;

class UniqueTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    /**
     * @var int
     */
    private $actualValue;
    /**
     * @var int
     */
    private $wrongValue;

    protected function createEntities()
    {
        $this->actualValue = 13;
        $this->wrongValue = 1;
        TestModel::create(['integer_field'=> $this->actualValue]);
    }

    public function testShouldPassIfThisValueNotExistInDatabase()
    {
        $unique = new Unique(TestModel::class);

        self::assertTrue($unique->passes('integer_field', $this->wrongValue));
    }

    public function testShouldFailIfValueExistsInDatabase()
    {
        $unique = new Unique(TestModel::class);

        self::assertFalse($unique->passes('integer_field', $this->actualValue));
    }
}
