<?php

namespace Ata\Cycle\ORM\Tests\Models\RelatedModels;

use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasOne;
use Cycle\Annotated\Annotation\Relation\Inverse;

/**
 * @Entity
 */
class RelatedModel extends BaseTestModel
{
    /** @HasOne(target=HasOneTestModel::class, inverse=@Inverse(type="belongsTo", as="has_one_inverse"), nullable=true) */
    public $has_one;
}
