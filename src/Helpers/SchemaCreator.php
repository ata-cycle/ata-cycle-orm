<?php


namespace Ata\Cycle\ORM\Helpers;


use Cycle\Bootstrap\Command\Generator\ShowChanges;
use Cycle\Schema;
use Cycle\Annotated;
use Cycle\ORM;
use Cycle\Migrations\GenerateMigrations;
use Doctrine\Common\Annotations\AnnotationRegistry;


class SchemaCreator
{
    public static function createSchema(array $generators = [], array $defaults = [])
    {
        $modelsPath = config('cycle.schema.path');
        if (!is_array($modelsPath)){
            $modelsPath = [$modelsPath];
        }

        foreach ($modelsPath as $path){
            if (!is_dir($path)){
                mkdir($path, 665, true);
            }
        }

        $finder = (new \Symfony\Component\Finder\Finder())->files()->in($modelsPath);
        $classLocator = new \Spiral\Tokenizer\ClassLocator($finder);

        // autoload annotations
        AnnotationRegistry::registerLoader('class_exists');

        if (empty($generators)) {
            $generators = config('cycle.schema.generators');
        }
        $generators = array_merge([
            new Annotated\Entities($classLocator),              // register annotated entities
            new Annotated\Embeddings($classLocator),            // register embeddable entities
        ], $generators);

        $generators = static::convertToObjects($generators);

        $schema = (new Schema\Compiler())->compile(new Schema\Registry(resolve('cycle-db.db-manager')),
            $generators, $defaults
        );

        return new ORM\Schema($schema);
    }

    private static function convertToObjects(array $generators)
    {
        $result = [];

        foreach ($generators as $generator){
            if (is_string($generator)){
                $result[] = new $generator;
            }
            elseif (is_object($generator)){
                $result[] = $generator;
            }
        }

        return $result;
    }
}
