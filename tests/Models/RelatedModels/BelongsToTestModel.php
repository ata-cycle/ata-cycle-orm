<?php

namespace Ata\Cycle\ORM\Tests\Models\RelatedModels;

use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;

/**
 * @Entity
 */
class BelongsToTestModel extends BaseTestModel
{

    public $belongs_to_inverse;
}
