<?php

namespace Ata\Cycle\ORM\Tests\Unit;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Illuminate\Support\Collection;

class FetchOneTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    protected function createEntities()
    {
        TestModel::create(['integer_field'=>1]);
        TestModel::create(['integer_field'=>2]);
        TestModel::create(['integer_field'=>3]);
        TestModel::create(['integer_field'=>4]);
    }

    public function testShouldCleanUpSelect()
    {
        Collection::times(10, function(){
            self::assertEquals(1, TestModel::where('integer_field', 1)->count());
            resolve('cycle-db.heap-clean');
        });
    }
}
