<?php

namespace Ata\Cycle\ORM;

use Ata\Cycle\ORM\Console\Commands as AtaCommands;
use Ata\Cycle\ORM\Factory\CycleFactory;
use Ata\Cycle\ORM\Helpers\SchemaCreator;
use Ata\Cycle\ORM\Repositories\FileRepository;
use Cycle\Bootstrap\Command as CycleCommands;
use Cycle\ORM\Factory;
use Cycle\ORM\Transaction;
use Illuminate\Support\ServiceProvider;
use Spiral\Database\Config\DatabaseConfig;
use Spiral\Database\DatabaseManager;
use Spiral\Migrations\Config\MigrationConfig;
use Spiral\Migrations\Migrator;
use Cycle\ORM;

class PackageServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMigrator();

        $this->registerCycleDb();

        if (config('cycle.factory.use'))
        {
            $this->registerFactory();
        }
    }

    public function boot()
    {

        $this->publishes([
            __DIR__ . '/../config/cycle.php' => config_path('cycle.php')
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/../config/cycle.php', 'cycle'
        );

        if ($this->app->runningInConsole())
        {
            $this->commands([
                AtaCommands\Migrate::class,
                AtaCommands\Rollback::class,
                AtaCommands\Configure::class,
                AtaCommands\Refresh::class,
                AtaCommands\Fresh::class,
                AtaCommands\MakeMigration::class,
                CycleCommands\ListCommand::class,
                CycleCommands\SyncCommand::class,
                CycleCommands\UpdateCommand::class,
                CycleCommands\Database\ListCommand::class,
                CycleCommands\Database\TableCommand::class,
            ]);
        }
    }

    protected function registerMigrator()
    {
        $this->app->singleton('cycle-db.migrator.config', function ($app) {
            return new MigrationConfig(config('cycle.migrations'));
        });

        $this->app->singleton('cycle-db.migrator', function ($app) {
            return new Migrator($app['cycle-db.migrator.config'], $app['cycle-db.db-manager'], new FileRepository($app['cycle-db.migrator.config']));
        });

        $this->app->singleton(Migrator::class, 'cycle-db.migrator');
    }

    protected function registerCycleDb()
    {
        // The connection factory is used to create the actual connection instances on
        // the database. We will inject the factory into the manager so that it may
        // make the connections while they are actually needed and not of before.
        $this->app->singleton('cycle-db.db-manager', function ($app) {
            return new DatabaseManager(
                new DatabaseConfig(config('cycle.database'))
            );
        });

        // The database manager is used to resolve various connections, since multiple
        // connections might be managed. It also implements the connection resolver
        // interface which may be used by other components requiring connections.
        $this->app->bind('cycle-db.db-factory', function ($app) {

            return function ($generators = []) use($app) {
                return (new ORM\ORM((new Factory($app['cycle-db.db-manager']))->withDefaultSchemaClasses([
                    ORM\SchemaInterface::MAPPER => config('cycle.schema.mapper'),
                    ORM\SchemaInterface::REPOSITORY => config('cycle.schema.repository'),
                    ORM\SchemaInterface::SOURCE => config('cycle.schema.source'),
                    ORM\SchemaInterface::CONSTRAIN => config('cycle.schema.constrain'),
                ])))->withSchema(SchemaCreator::createSchema($generators, [
                    ORM\SchemaInterface::MAPPER => config('cycle.schema.mapper'),
                    ORM\SchemaInterface::REPOSITORY => config('cycle.schema.repository'),
                    ORM\SchemaInterface::SOURCE => config('cycle.schema.source'),
                    ORM\SchemaInterface::CONSTRAIN => config('cycle.schema.constrain'),
                ]))->withPromiseFactory(new ORM\Promise\PromiseFactory());
            };
        });

        $this->app->singleton('cycle-db', function ($app) {
            return $app['cycle-db.db-factory']();
        });

        $this->app->bind('cycle-db.transaction', function ($app) {
            return new Transaction($app['cycle-db']);
        });

        $this->app->bind('cycle-db.heap-clean', function ($app) {
            resolve('cycle-db')->getHeap()->clean();
        });
    }

    protected function registerFactory()
    {
        $this->app->singleton(\Illuminate\Database\Eloquent\Factory::class, CycleFactory::class);
    }
}
