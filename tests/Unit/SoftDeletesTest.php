<?php

namespace Ata\Cycle\ORM\Tests\Unit;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Ata\Cycle\ORM\Tests\Models\TestSoftDeleteModel;

class SoftDeletesTest extends BaseDBTestCase
{

    protected function getSourceClass()
    {
        return TestModel::class;
    }

    protected function createEntities()
    {
        TestSoftDeleteModel::create(['integer_field' => 1]);
        TestSoftDeleteModel::create(['integer_field' => 2]);
        TestSoftDeleteModel::create(['integer_field' => 3]);
        TestSoftDeleteModel::create(['integer_field' => 4]);
        TestSoftDeleteModel::create(['integer_field' => 5]);
    }

    protected function dropTables(): bool
    {
        return false;
    }

    protected function withTimestamps()
    {
        return true;
    }

    public function testShouldCorrectAddDeletedAtWithExistedEntities()
    {
        $modelId = TestSoftDeleteModel::create(['integer_field' => 10])->id;

        TestSoftDeleteModel::firstOrFail($modelId)->delete();

        self::assertNull(TestSoftDeleteModel::findByPk($modelId));
    }
}
