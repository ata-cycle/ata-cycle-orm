<?php

namespace Ata\Cycle\ORM\Tests\Models\RelatedModels;

use Ata\Cycle\ORM\Models\Traits\IntPrimary;
use Cycle\Annotated\Annotation\Entity;

/**
 * @Entity
 */
class PivotTestModel
{
    use IntPrimary;
}
