<?php


namespace Ata\Cycle\ORM\Console\Commands\Traits;


use Spiral\Migrations\MigrationInterface;

trait GetMigrationFileName
{
    public function getMigrationFileName(MigrationInterface $migration)
    {
        $name = $migration->getState()->getName();
        $created = $migration->getState()->getTimeCreated()->format(config('cycle.migrations.timestamp_format'));

        return $created . '_' . $name;
    }
}
