<?php

namespace Ata\Cycle\ORM\Tests\Unit\SaveTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestManyToManyModel;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Ata\Cycle\ORM\Tests\Models\TestRelatedModel;

class ManyToManyTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    /**
     * @var TestModel
     */
    private $testModel;

    protected function createEntities()
    {
        $this->testModel = (new TestManyToManyModel([
            'integer_field' => 4,
            'related' => [
                new TestRelatedModel(['string_field' => 'asdf1']),
                new TestRelatedModel(['string_field' => 'asdf2']),
                new TestRelatedModel(['string_field' => 'asdf3']),
            ]]))->save();
    }

    public function testShouldFilterEntitiesViaArrayOfIds()
    {
        $data = [
            'related' => TestRelatedModel::orderBy('id')
                ->limit(2)
                ->findAll()
                ->slice(0, 2)
                ->map(function ($elem) {
                    return $elem->id;
                })->toArray()
        ];

        $testModel = TestManyToManyModel::findOne();

        $testModel->updateRelations($data);

        resolve('cycle-db.heap-clean');

        $testModel = TestManyToManyModel::load('related', [
            'load' => function ($qb){
                $qb->orderBy('id');
            }
        ])->findOne();

        self::assertEquals(2, $testModel->related->count());

        self::assertEquals($data['related'][0], $testModel->related[0]->id);
        self::assertEquals('asdf1', $testModel->related[0]->string_field);
        self::assertEquals($data['related'][1], $testModel->related[1]->id);
        self::assertEquals('asdf2', $testModel->related[1]->string_field);
    }

    public function testShouldAddEntitiesViaArrayOfIds()
    {
        $newId = (new TestRelatedModel(['string_field'=>'asdf4']))->save()->id;

        $data = [
            'related' => collect(TestRelatedModel::orderBy('id')
                ->limit(2)
                ->fetchAll())
                ->slice(0, 2)
                ->map(function ($elem) {
                    return $elem->id;
                })
                ->add($newId)
                ->toArray()
        ];

        $testModel = TestManyToManyModel::findOne();

        $testModel->updateRelations($data);

        resolve('cycle-db.heap-clean');

        $testModel = TestManyToManyModel::load('related')->fetchOne();

        self::assertEquals(3, $testModel->related->count());

        $filter = $testModel->related->filter(function ($elem) use($newId) {
            return $elem->id == $newId;
        });

        self::assertEquals(1, $filter->count());

        foreach ($filter as $elem){
            self::assertEquals('asdf4', $elem->string_field);
        }
    }
}
