<?php

namespace Ata\Cycle\ORM\Testing;

use Ata\Cycle\ORM\Testing\Traits\DatabaseLogger;
use Ata\Cycle\ORM\Testing\Traits\DatabaseTransactions;
use Ata\Cycle\ORM\Testing\Traits\TruncateTables;
use Illuminate\Foundation\Testing\TestCase;

abstract class BaseLaravelTestCase extends TestCase
{
    protected function setUpTraits()
    {
        $uses = parent::setUpTraits();

        if (isset($uses[DatabaseLogger::class])) {
            $this->setupDatabaseLogger();
        }

        if (isset($uses[TruncateTables::class])) {
            $this->setupTruncateTables();
        }

        if (isset($uses[DatabaseTransactions::class])) {
            $this->setupDatabaseTransactions();
        }

        return $uses;
    }
}
