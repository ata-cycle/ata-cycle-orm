# Базовая модель

По умолчанию используются аннотации для того, чтобы создать схему сущностей в БД.

Для того, чтобы работали методы ActiveRecord и методы, схожие по сигнатуре с методами Laravel, 
необходимо отнаследоваться от базового класса `CycleModel`.

Этот класс предоставляет следующие возможности:

## Создание модели с подмоделями путем передачи ассоциативного массива в конструктор

В этом примере мы создаем сущность со строковым полем и сохраняем ее в БД.

```php
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Ata\Cycle\ORM\Models\CycleModel;
use Ata\Cycle\ORM\Traits\IntPrimary;

/**
 * @Entity
*/
class MyModel extends CycleModel
{ 
    use IntPrimary;
    /**
     * @Column(type="string")
    */
    public $string_field;
}

MyModel::create(['string_field' => 'hello world']);
```

В этом примере мы создаем сущность с несколькими вложенными сущностями и сохраняем их в БД в рамках одной транзакции.

```php
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Ata\Cycle\ORM\Models\CycleModel;

/**
 * @Entity
*/
class MyModel extends CycleModel
{ 
    /**
     * @Column(type="string")
    */
    public $string_field;

    /**
     * @HasMany(target=MyRelatedModel::class)
    */
    public $related;
}

/**
 * @Entity
*/
class MyRelatedModel extends CycleModel
{ 
    /**
     * @Column(type="integer")
    */
    public $integer_field;
}

MyModel::create([ 
    'string_field' => 'hello world',
    'related' => [
        ['integer_field' => 1],
        ['integer_field' => 2],
    ], 
]);
```

## Заполнение данных из входящего ассоциативного массива

Метод `fill()` вызывается при вызове конструктора, так что предыдущие примеры показывают, как он работает.

## Обновление дочерних связей по приходящему ассоциативному массиву

В данный момент доступно только обновление связей первого уровня, без углублений в рекурсию.

Для этого необходимо вызвать метод `updateRelations()`.

```
MyModel::findByPK(1)->updateRelations(['related' => [1,2,3]]);
```

В этом примере к объекту будут привязаны дочерние сущности с ИДами `1, 2, 3`, если они присутствуют в БД. Если же их в БД нет, то у объекта не будет каких-либо дочерних сущностей.

## Удаление сущности

Удаление производится методом `delete()`

```php
MyModel::findByPK(1)->delete();
```

По умолчанию дочерние сущности удаляются вместе с родительскими, чтобы этого не происходило, надо передать флаг `false`

```php
MyModel::findByPK(1)->delete(false);
```

## Построение запросов в БД

Можно вызывать методы репозитория, не обращаясь к самому репозиторию

```php
MyModel::findByPK(1);
MyModel::findOne();
MyModel::findAll();
```

-----
Прочие методы модели можно посмотреть в `src/Models/Interfaces/CycleModelInterface.php`



# Трейты

Существует набор предустановленных трейтов

| Название | Описание |
|---|---|
|`IntPrimary` | добавляет поле `id` с типом `int`|
| `SoftDeletes` | добавляет поле `deleted_at` и несколько предустановленных функций. **Не работает без замены команды маппера `Delete` на `SoftDeletes`!** |
| `StringPrimary` | добавляет поле `id` с типом `int`|
|`Timestamps` | добавляет поля `created_at`, `updated_at` с типом `datetime`. **Не работает без добавления команд маппера `Update\Timestamps` и `Create\Timestamps`!** |
