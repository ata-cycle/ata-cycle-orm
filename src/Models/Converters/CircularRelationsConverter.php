<?php


namespace Ata\Cycle\ORM\Models\Converters;


use Cycle\ORM\Promise\Collection\CollectionPromise;
use Cycle\ORM\Promise\PromiseMany;
use Cycle\ORM\Promise\PromiseOne;
use Cycle\ORM\Promise\Reference;
use Cycle\ORM\Relation;
use Cycle\ORM\Relation\Pivoted\PivotedCollection;
use Cycle\ORM\Relation\Pivoted\PivotedCollectionPromise;
use Cycle\ORM\SchemaInterface;
use Doctrine\Common\Collections\ArrayCollection;

class CircularRelationsConverter
{

    /**
     * Array of visited objects; used to prevent cycling.
     *
     * @var array
     */
    protected $visited = [];

    public function extract(?object $object): ?object
    {
        if ($this->wasVisited($object) || $object === null) {
            return null;
        }

        if ($object instanceof Reference or $object instanceof PromiseOne) {
            return null;
        }

        $this->visited[] = $object;

        $result = clone $object;

        /** @var $schema SchemaInterface */
        $schema = resolve('cycle-db')->getSchema();
        $relations = $schema->getRelations(get_class($object));

        foreach ($relations as $relationName) {
            $relation = $schema->defineRelation(get_class($object), $relationName);

            switch ($relation[Relation::TYPE]) {
                case Relation::BELONGS_TO:
                case Relation::HAS_ONE:
                case Relation::REFERS_TO:
                    $result->{$relationName} = $this->extract($object->{$relationName});
                    break;
                case Relation::MANY_TO_MANY:
                    $result->{$relationName} = new PivotedCollection();

                    if ($object->{$relationName} === null) {
                        break;
                    }

                    if ($object->{$relationName} instanceof PivotedCollectionPromise or $object->{$relationName} instanceof PromiseMany) {
                        break;
                    }

                    foreach ($object->{$relationName} as $entity) {
                        $extract = $this->extract($entity);
                        if ($extract !== null) {
                            $result->{$relationName}->add($extract);
                        }
                    }
                    break;
                case Relation::HAS_MANY:
                    $result->{$relationName} = new ArrayCollection();

                    if ($object->{$relationName} === null) {
                        break;
                    }

                    if ($object->{$relationName} instanceof CollectionPromise or $object->{$relationName} instanceof PromiseMany) {
                        break;
                    }

                    foreach ($object->{$relationName} as $entity) {
                        $extract = $this->extract($entity);
                        if ($extract !== null) {
                            $result->{$relationName}->add($extract);
                        }
                    }
                    break;
            }
        }

        return $result;
    }

    /**
     * Determine if an object has been serialized already.
     *
     * @param mixed $value
     * @return bool
     */
    protected function wasVisited(&$value)
    {
        return in_array($value, $this->visited, true);
    }
}
