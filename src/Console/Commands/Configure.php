<?php


namespace Ata\Cycle\ORM\Console\Commands;


use Illuminate\Console\Command;
use Spiral\Migrations\Migrator;

class Configure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'cycle:install';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the migration table';

    /**
     * @var Migrator
     */
    private $migrator;

    /**
     * Create a new command instance.
     * @param Migrator $migrator
     */
    public function __construct(Migrator $migrator)
    {
        parent::__construct();
        $this->migrator = $migrator;
    }

    public function handle()
    {
        if (!$this->migrator->isConfigured()) {
            $this->migrator->configure();
            $this->info('Cycle migrations successfully configured');
        } else {
            $this->info('Cycle migrations already configured');
        }
    }
}
