<?php


namespace Ata\Cycle\ORM\Console\Commands;


use Ata\Cycle\ORM\Generators\GenerateMigrations;
use Ata\Cycle\ORM\Generators\ShowChanges;
use Illuminate\Console\Command;
use Spiral\Migrations\Migrator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'cycle:make-migration';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create migration with changes between code and database';
    /**
     * @var Migrator
     */
    private $migrator;

    /**
     * Create a new command instance.
     * @param Migrator $migrator
     */
    public function __construct(Migrator $migrator)
    {
        parent::__construct();
        $this->migrator = $migrator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        if (!$this->migrator->isConfigured()){
            $this->migrator->configure();
        }

        resolve('cycle-db.db-factory')(
            array_merge(config('cycle.schema.generators'), [
                new ShowChanges($this->output),
                new GenerateMigrations(
                    resolve('cycle-db.migrator')->getRepository(),
                    resolve('cycle-db.migrator.config'),
                    $this->input->getArgument('name'),
                    $this->output,
                    $this->input->getOption('empty'),
                ),])
        );

        return;
    }

    protected function getArguments()
    {
        return [
            ['name', InputArgument::OPTIONAL, 'Name of migration, default value in config'],
        ];
    }

    protected function getOptions()
    {
        return [
            ['empty', 'e', InputOption::VALUE_NONE, 'generate empty migration'],
        ];
    }
}
