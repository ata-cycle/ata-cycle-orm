<?php

namespace Ata\Cycle\ORM\Tests\TypecastTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;

class JsonTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public function testShouldSaveToDatabaseJsonField()
    {
        $testModel = new TestModel(['json_field' => ['test' => 'asdf']]);
        $testModel->save();

        self::assertTrue($testModel->json_field->offsetExists('test'));
        self::assertEquals('asdf', $testModel->json_field['test']);
    }

    public function testShouldRetrieveFromDatabaseJsonField()
    {
        $testModel = new TestModel(['json_field' => ['test' => 'asdf']]);
        $testModel->save();

        resolve('cycle-db.heap-clean');

        $testModel = TestModel::firstOrFail();

        self::assertTrue($testModel->json_field->offsetExists('test'));
        self::assertEquals('asdf', $testModel->json_field['test']);
    }
}
