<?php


namespace Ata\Cycle\ORM\Console\Commands;


use Ata\Cycle\ORM\Console\Commands\Traits\GetMigrationFileName;
use Illuminate\Console\Command;
use Spiral\Migrations\Migrator;

class Refresh extends Command
{
    use GetMigrationFileName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'cycle:refresh';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rollback and re-run all migrations';

    /**
     * @var Migrator
     */
    private $migrator;

    /**
     * Create a new command instance.
     * @param Migrator $migrator
     */
    public function __construct(Migrator $migrator)
    {
        parent::__construct();
        $this->migrator = $migrator;
    }

    public function handle()
    {
        $this->call('db:wipe');
        $this->call('cycle:migrate');
    }
}
