<?php

declare(strict_types=1);

namespace Ata\Cycle\ORM\Repositories;

use Cycle\ORM\RepositoryInterface;
use Cycle\ORM\Select\ConstrainInterface;
use Illuminate\Support\Collection;

interface AtaRepositoryInterface extends RepositoryInterface
{
    /**
     * Find entity by the primary key value or return null.
     *
     * @param mixed $id
     * @return AtaRepositoryInterface
     */
    public function findByPK($id): ?object;

    /**
     * Find entity using given scope (where).
     *
     * @param array $scope
     * @return null|object
     */
    public function findOne(array $scope = []): ?object;

    /**
     * Find multiple entities using given scope and sort options.
     *
     * @param array $scope
     * @return Collection
     */
    public function findAll(array $scope = []): iterable;

    /**
     * @param array $scope
     * @return Collection
     */
    function findOrFail(array $scope = []): Collection;

    /**
     * @param null $id
     * @return object
     */
    function firstOrFail($id = null): object;

    /**
     * @param array $defaultData
     * @param array $requiredData
     * @return object
     */
    function firstOrCreate(array $defaultData = [], array $requiredData = []): object;

    /**
     * @param array $updatingData
     * @param array $requiredData
     * @return Collection
     */
    function updateOrCreate(array $updatingData = [], array $requiredData = []): Collection;

    /**
     * @param $relation
     * @param array $options
     * @return mixed
     */
    function loadLeftJoin($relation, array $options = []): AtaRepositoryInterface;

    /**
     * @return AtaRepositoryInterface
     */
    function withoutConstrains(): AtaRepositoryInterface;

    /**
     * @param ConstrainInterface $constrain
     * @return AtaRepositoryInterface
     */
    function withConstrain(ConstrainInterface $constrain): AtaRepositoryInterface;

    /**
     * @return bool
     */
    function exists(): bool;

    /**
     * @return bool
     */
    function notExists(): bool;

    /**
     * @param $predicate
     * @param mixed ...$arguments
     * @return AtaRepositoryInterface
     */
    function whereIf($predicate, ...$arguments): AtaRepositoryInterface;

    function paginate(int $perPage, int $pageNumber = 0): AtaRepositoryInterface;
}
