<?php

return [
    'migrations' => [
        'directory' => database_path('cycle_migrations'),  // where to store migrations
        'table' => 'cycle_migrations',                 // database table to store migration status
        'default_migration_name' => 'auto',                 // database table to store migration status
        'timestamp_format' => 'Y-m-d.H-i-s',                 // database table to store migration status
    ],

    'database' => [
        'default' => 'default',
        'databases' => [
            'default' => ['connection' => env('DB_CONNECTION', 'sqlite')],
        ],
        'connections' => [
            'sqlite' => [
                'driver' => \Spiral\Database\Driver\SQLite\SQLiteDriver::class,
                'connection' => 'sqlite:' . database_path('database.sqlite'),
                'username' => '',
                'password' => '',
            ],
            'mysql' => [
                'driver' => \Spiral\Database\Driver\MySQL\MySQLDriver::class,
                'options' => [
                    'connection' => 'mysql:host=' . env('DB_HOST', '127.0.0.1') . ';dbname=' . env('DB_DATABASE', 'laravel') . ';mysql:port=' . env('DB_PORT', '3306'),
                    'username' => env('DB_USERNAME', 'root'),
                    'password' => env('DB_PASSWORD', ''),
                ]
            ],
            'pgsql' => [
                'driver' => \Ata\Cycle\ORM\Drivers\PostgresDriver::class,
                'options' => [
                    'connection' => 'pgsql:host=' . env('DB_HOST', '127.0.0.1') . ';dbname=' . env('DB_DATABASE', 'laravel') . ';pgsql:port=' . env('DB_PORT', '5432'),
                    'username' => env('DB_USERNAME', 'root'),
                    'password' => env('DB_PASSWORD', ''),
                ]
            ],
            'sqlServer' => [
                'driver' => \Spiral\Database\Driver\SQLServer\SQLServerDriver::class,
                'options' => [
                    'connection' => 'sqlsrv:Server=OWNER;Database=DATABASE',
                    'username' => 'sqlServer',
                    'password' => 'sqlServer',
                ],
            ],
        ]
    ],

    'factory' => [
        'use' => true,
        'factory_class' => \Ata\Cycle\ORM\Factory\CycleFactory::class,
        'builder_class' => \Ata\Cycle\ORM\Factory\CycleFactoryBuilder::class,
    ],

// where to find models, write folders with relative path
    'schema' => [
        'generators' => [
            \Cycle\Schema\Generator\ResetTables::class,       // re-declared table schemas (remove columns)
            \Cycle\Annotated\MergeColumns::class,             // register non field columns (table level)
            \Cycle\Schema\Generator\GenerateRelations::class, // generate entity relations
            \Cycle\Schema\Generator\ValidateEntities::class,  // make sure all entity schemas are correct
            \Cycle\Schema\Generator\RenderTables::class,      // declare table schemas
            \Cycle\Schema\Generator\RenderRelations::class,   // declare relation keys and indexes
            \Cycle\Schema\Generator\GenerateTypecast::class,  // typecast non string columns
            \Cycle\Annotated\MergeIndexes::class,             // register non entity indexes (table level
        ],

        'path' => [
            base_path('app/Models'),
        ],

        'mapper' => \Ata\Cycle\ORM\Mappers\AtaMapper::class,
        'repository' => \Ata\Cycle\ORM\Repositories\AtaRepository::class,
        'source' => \Cycle\ORM\Select\Source::class,
        'constrain' => null,
    ],

    'commands' => [
        'create'  => [
            \Ata\Cycle\ORM\Mappers\Commands\Create\Create::class,
        ],
        'update'  => [
            \Ata\Cycle\ORM\Mappers\Commands\Update\Update::class,
        ],
        'delete'  => [
            \Ata\Cycle\ORM\Mappers\Commands\Delete\Delete::class,
        ],

    ],

    'testing' => [
        'source_class' => ''
    ]
];
