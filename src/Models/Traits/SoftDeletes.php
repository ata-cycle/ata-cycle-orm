<?php


namespace Ata\Cycle\ORM\Models\Traits;


use Ata\Cycle\ORM\Repositories\AtaRepositoryInterface;
use Cycle\Annotated\Annotation\Column;
use Cycle\ORM\Heap\Node;
use Ata\Cycle\ORM\Typecasts\Carbon;

trait SoftDeletes
{
    /**  @Column(type="datetime", nullable=true, typecast=Carbon::class) */
    public $deleted_at;

    public function forceDelete()
    {
        resolve('cycle-db')->getHeap()->get($this)->setStatus(Node::SCHEDULED_DELETE);

        $this->delete();
    }

    public static function restoreById($id): object
    {
        $entity = static::withoutConstrains()->findByPK($id);
        return $entity->restore();
    }

    public function restore(): object
    {
        $this->deleted_at = null;
        return $this->save();
    }

    public static function trash(): AtaRepositoryInterface
    {
        return static::withoutConstrains()->where('deleted_at', '!=', null);
    }

    public static function withTrash(): AtaRepositoryInterface
    {
        return static::withoutConstrains();
    }
}
