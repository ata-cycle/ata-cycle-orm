# Тестирование

##Тестирование пакетов

Для тестирования пакетов необходимо отнаследоваться от класса

`Ata\Cycle\ORM\Testing\BaseDBTestCase`.

Этот класс, в свою очередь, наследуется от `Orchestra\Testbench\TestCase`, поэтому всё, 
что описано в документации [Orchestra](https://github.com/orchestral/testbench), применимо и к этому классу.

Однако, есть часть изменений в цикле `setUp`-`tearDown`.

По умолчанию все тестовые модели находятся в папке `tests/Models`, а миграции - в папке `tests/cycle_migrations`.

Цикл подготовки к тесту включает в себя следующие команды:

1. Определение разницы между тестовыми моделями и их схемой в БД и установка миграции
1. Создание сущностей через переопределение `protected` метода `createEntities`.

Цикл завершения теста состоит из следующих команд:

1. Удаление данных из всех таблиц в БД (включено по умолчанию)
1. Откат миграций и удаление файлов миграций (отключено по умолчанию)


Для того, чтобы корректно работало удаление данных из БД, необходимо в абстрактной функции `getSourceClass`
вернуть класс модели, использующейся в тесте. 

Так же возможно включить логгер запросов в БД функцией `enableLog` (ее необходимо вызвать в setUp или в начале теста).

Если приложению для работы нужны дополнительные `ServiceProvider` для корректной работы приложения, тогда необходимо их 
записать в методе `getPackageProviders`.


## Тестирование в Laravel-проекте

Для feature-тестирования нужно отнаследоваться от класса

`Ata\Cycle\ORM\BaseLaravelTestCase`.

Присутствует несколько полезных трейтов:

1. `TruncateTables` - очищает всю тестовую БД после каждого теста
1. `DatabaseLogger` - добавляет возможность просмотра логов запросов в БД.

К сожалению, на текущий момент работа трейта `DatabaseTransactions` некорректна и необходимо 
использовать отдельную БД для тестов. 
