<?php


namespace Ata\Cycle\ORM\Models\Traits;


use Cycle\ORM\Exception\MapperException;
use Ramsey\Uuid\Uuid;

trait UuidNextKey
{
    /**
     * Generate entity primary key value.
     */
    public function nextPrimaryKey()
    {
        try {
            return Uuid::uuid4()->toString();
        } catch (\Exception $e) {
            throw new MapperException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
