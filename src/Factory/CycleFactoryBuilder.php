<?php

namespace Ata\Cycle\ORM\Factory;

use Ata\Cycle\ORM\Models\CycleModel;
use Illuminate\Database\Eloquent\FactoryBuilder;

class CycleFactoryBuilder extends FactoryBuilder
{
    /**
     * Create a collection of models and persist them to the database.
     *
     * @param  array  $attributes
     * @return \Illuminate\Database\Eloquent\Collection|CycleModel|mixed
     */
    public function create(array $attributes = [])
    {
        $results = $this->make($attributes);

        if ($results instanceof CycleModel) {
            $this->store(collect([$results]));

            $this->callAfterCreating(collect([$results]));
        } else {
            $this->store($results);

            $this->callAfterCreating($results);
        }

        return $results;
    }

    protected function store($results)
    {
        $results->each(function ($model) {
            $model->save();
        });
    }
}
