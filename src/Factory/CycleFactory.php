<?php

namespace Ata\Cycle\ORM\Factory;

use Illuminate\Database\Eloquent\Factory;

class CycleFactory extends Factory
{
    /**
     * Create a builder for the given model.
     *
     * @param  string  $class
     * @param  string  $name
     * @return \Illuminate\Database\Eloquent\FactoryBuilder
     */
    public function of($class, $name = 'default')
    {
        $builderClass = config('cycle.factory.builder_class');
        return new $builderClass(
            $class, $name, $this->definitions, $this->states,
            $this->afterMaking, $this->afterCreating, $this->faker
        );
    }
}
