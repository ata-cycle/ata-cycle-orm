<?php

namespace Ata\Cycle\ORM\Mappers\Commands\Delete;

use Ata\Cycle\ORM\Mappers\Commands\Interfaces\DeleteMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Ata\Cycle\ORM\Models\Traits\SoftDeletes;
use Carbon\CarbonImmutable;
use Cycle\ORM\Command\CommandInterface;
use Cycle\ORM\Command\Database\Update;
use Cycle\ORM\Context\ConsumerInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class SoftDelete implements DeleteMapperCommand
{

    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?CommandInterface $previousCommand): ?CommandInterface
    {
        $uses = array_flip(class_uses_recursive($entity));

        if ($state->getStatus() == Node::SCHEDULED_DELETE || !isset($uses[SoftDeletes::class])) {
            return $mapper->defaultDelete($entity, $node, $state);
        }
        // identify entity as being "deleted"
        $state->setStatus(Node::SCHEDULED_DELETE);
        $state->decClaim();

        $cmd = new Update(
            $mapper->getSource()->getDatabase(),
            $mapper->getSource()->getTable(),
            ['deleted_at' => new CarbonImmutable()]
        );

        // forward primaryKey value from entity state
        // this sequence is only required if the entity is created and deleted
        // within one transaction
        $cmd->waitScope($mapper->getPrimaryColumn());
        $state->forward(
            $mapper->getPrimaryKey(),
            $cmd,
            $mapper->getPrimaryColumn(),
            true,
            ConsumerInterface::SCOPE
        );

        return $cmd;
    }
}
