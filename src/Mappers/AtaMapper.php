<?php


namespace Ata\Cycle\ORM\Mappers;


use Ata\Cycle\ORM\Models\Traits\ModelEvents;
use Cycle\ORM\Command\CommandInterface;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;
use Cycle\ORM\Mapper\Mapper;
use Cycle\ORM\Schema;
use Cycle\ORM\Select\SourceInterface;


class AtaMapper extends Mapper implements DefaultMapperInterface
{

    public function init(array $data): array
    {
        $class = $this->orm->getSchema()->define($this->role, Schema::ENTITY);

        // return empty entity and prepared data
        return [new $class, $data];
    }

    /**
     * Proxy function for parent::queueCreate
     * @param $entity
     * @param Node $node
     * @param State $state
     * @return ContextCarrierInterface
     */
    public function defaultCreate($entity, Node $node, State $state): ContextCarrierInterface
    {
        return parent::queueCreate($entity, $node, $state);
    }

    /**
     * Proxy function for parent::queueUpdate
     * @param $entity
     * @param Node $node
     * @param State $state
     * @return ContextCarrierInterface
     */
    public function defaultUpdate($entity, Node $node, State $state): ContextCarrierInterface
    {
        return parent::queueUpdate($entity, $node, $state);
    }

    /**
     * Proxy function for parent::queueDelete
     * @param $entity
     * @param Node $node
     * @param State $state
     * @return CommandInterface
     */
    public function defaultDelete($entity, Node $node, State $state): CommandInterface
    {
        return parent::queueDelete($entity, $node, $state);
    }

    /**
     * Proxy function for $this->>fetchFields
     * @param $entity
     * @return array
     */
    function fetchEntityFields($entity): array
    {
        return $this->fetchFields($entity);
    }


    public function queueCreate($entity, Node $node, State $state): ContextCarrierInterface
    {
        $cmd = null;

        $uses = array_flip(class_uses_recursive($entity));

        if (isset($uses[ModelEvents::class])) {
            foreach ($entity->beforeCreate() as $command) {
                if (is_string($command)){
                    $command = new $command;
                }

                $cmd = $command->run($this, $entity, $node, $state, $cmd);
            }
        }

        foreach (config('cycle.commands.create') as $command) {
            if (is_string($command)){
                $command = new $command;
            }
            $cmd = $command->run($this, $entity, $node, $state, $cmd);
        }

        if (isset($uses[ModelEvents::class])) {
            foreach ($entity->afterCreate() as $command) {
                if (is_string($command)){
                    $command = new $command;
                }
                $cmd = $command->run($this, $entity, $node, $state, $cmd);
            }
        }

        return $cmd;
    }

    public function queueUpdate($entity, Node $node, State $state): ContextCarrierInterface
    {
        $cmd = null;

        $uses = array_flip(class_uses_recursive($entity));

        if (isset($uses[ModelEvents::class])) {
            foreach ($entity->beforeUpdate() as $command) {
                if (is_string($command)){
                    $command = new $command;
                }
                $cmd = $command->run($this, $entity, $node, $state, $cmd);
            }
        }
        foreach (config('cycle.commands.update') as $command) {
            if (is_string($command)){
                $command = new $command;
            }
            $cmd = $command->run($this, $entity, $node, $state, $cmd);
        }
        if (isset($uses[ModelEvents::class])) {
            foreach ($entity->afterUpdate() as $command) {
                if (is_string($command)){
                    $command = new $command;
                }
                $cmd = $command->run($this, $entity, $node, $state, $cmd);
            }
        }

        return $cmd;
    }

    public function queueDelete($entity, Node $node, State $state): CommandInterface
    {
        $cmd = null;

        $uses = array_flip(class_uses_recursive($entity));

        if (isset($uses[ModelEvents::class])) {
            foreach ($entity->beforeDelete() as $command) {
                if (is_string($command)){
                    $command = new $command;
                }
                $cmd = $command->run($this, $entity, $node, $state, $cmd);
            }
        }
        foreach (config('cycle.commands.delete') as $command) {
            if (is_string($command)){
                $command = new $command;
            }
            $cmd = $command->run($this, $entity, $node, $state, $cmd);
        }
        if (isset($uses[ModelEvents::class])) {
            foreach ($entity->afterDelete() as $command) {
                if (is_string($command)){
                    $command = new $command;
                }
                $cmd = $command->run($this, $entity, $node, $state, $cmd);
            }
        }

        return $cmd;
    }

    function getPrimaryKey(): string
    {
        return $this->primaryKey;
    }

    function getPrimaryColumn(): string
    {
        return $this->primaryColumn;
    }

    function getSource(): SourceInterface
    {
        return $this->source;
    }
}
