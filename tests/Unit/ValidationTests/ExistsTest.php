<?php

namespace Ata\Cycle\ORM\Tests\Unit\ValidationTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Ata\Cycle\ORM\Validation\Exists;

class ExistsTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    /**
     * @var int
     */
    private $actualValue;
    /**
     * @var int
     */
    private $wrongValue;

    protected function createEntities()
    {
        $this->actualValue = 13;
        $this->wrongValue = 1;
        TestModel::create(['integer_field'=> $this->actualValue]);
    }

    public function testShouldFailIfThisValueNotExistInDatabase()
    {
        $exists = new Exists(TestModel::class, 'integer_field');

        self::assertFalse($exists->passes('', $this->wrongValue));
    }

    public function testShouldPassIfValueExistsInDatabase()
    {
        $exists = new Exists(TestModel::class, 'integer_field');

        self::assertTrue($exists->passes('', $this->actualValue));
    }
}
