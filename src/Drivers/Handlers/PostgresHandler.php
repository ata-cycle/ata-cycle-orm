<?php

namespace Ata\Cycle\ORM\Drivers\Handlers;

use Spiral\Database\Schema\AbstractTable;

class PostgresHandler extends \Spiral\Database\Driver\Postgres\PostgresHandler
{
    public function dropTable(AbstractTable $table): void
    {
        $this->run(
            "DROP TABLE {$this->identify($table->getInitialName())} CASCADE"
        );
    }
}
