<?php

namespace Ata\Cycle\ORM\Tests\Unit\TypecastTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Carbon\CarbonImmutable;

class CarbonTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public function testShouldSaveFieldWithCarbonTypecast()
    {
        $testModel = new TestModel(['datetime_field' => new CarbonImmutable()]);
        $testModel->save();

        self::assertInstanceOf(CarbonImmutable::class, $testModel->datetime_field);
        self::assertNotNull($testModel->datetime_field);
    }

    public function testShouldRetrieveFieldWithCarbonTypecast()
    {
        $testModel = new TestModel(['datetime_field' => new CarbonImmutable()]);
        $testModel->save();

        resolve('cycle-db.heap-clean');

        $testModel = TestModel::firstOrFail();

        self::assertInstanceOf(CarbonImmutable::class, $testModel->datetime_field);
        self::assertNotNull($testModel->datetime_field);
    }
}
