<?php

namespace Ata\Cycle\ORM\Tests\Models\RelatedModels;

use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\BelongsTo;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Relation\Inverse;
use Cycle\Annotated\Annotation\Relation\ManyToMany;

/**
 * @Entity
 */
class ManyToManyTestModel extends BaseTestModel
{
    /**
     * @ManyToMany(though=PivotTestModel::class, target=HasManyTestModel::class, cascade=true)
     */
    public $many_to_many_inverse;

    /** @BelongsTo(target=BelongsToTestModel::class, nullable=true, inverse=@Inverse(type="hasMany", as="belongs_to_inverse")) */
    public $belongs_to;

    /** @HasMany(target=RelatedModel::class, nullable=true) */
    public $has_many;
}
