<?php


namespace Ata\Cycle\ORM\Testing\Traits;


use Ata\Cycle\ORM\Testing\TestLogger;

trait DatabaseLogger
{
    /**
     * @var TestLogger
     */
    private $testLogger;

    protected function disableLog()
    {
        $this->testLogger->hide();
    }

    protected function enableLog()
    {
        $this->testLogger->display();
    }

    public function setupDatabaseLogger()
    {
        $this->testLogger = new TestLogger();

        resolve('cycle-db')->getSource(config('cycle.testing.source_class'))
            ->getDatabase()->getDriver()->setLogger($this->testLogger);
    }
}
