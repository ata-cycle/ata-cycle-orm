<?php

namespace Ata\Cycle\ORM\Typecasts;

use ArrayAccess;
use Ata\Cycle\ORM\Models\Converters\Arrayable;
use Spiral\Database\DatabaseInterface;

class Json implements ArrayAccess, Arrayable
{
    /**
     * @var array
    */
    private $value;

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode($this->value);
    }

    /**
     * @param array $value
     * @return Json
     */
    public static function create($value = []): Json
    {
        $instance = new static();
        $instance->value = is_string($value) ? json_decode($value, true) : $value;
        return $instance;
    }

    /**
     * @param string            $value
     * @param DatabaseInterface $db
     * @return Json
     */
    public static function typecast($value, DatabaseInterface $db): Json
    {
        return static::create($value);
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->value[] = $value;
        } else {
            $this->value[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->value[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->value[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->value[$offset]) ? $this->value[$offset] : null;
    }

    public function toArray()
    {
        return $this->value;
    }
}
