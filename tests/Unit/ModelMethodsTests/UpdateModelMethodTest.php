<?php

namespace Ata\Cycle\ORM\Tests\Unit\ModelMethodsTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;

class UpdateModelMethodTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public $model;

    protected function createEntities(): void
    {
        $this->model = TestModel::create(['integer_field' => 123]);
    }

    /**
     * @throws \Throwable
     */
    public function testUpdateTest()
    {
        $updatedModel = TestModel::findOne()->update(['integer_field' => 111]);
        $this->assertEquals(111, $updatedModel->integer_field);
        $this->assertEquals($this->model->id, $updatedModel->id);
    }
}
