<?php

namespace Ata\Cycle\ORM\Tests\Models\RelatedModels;

use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\HasMany;
use Cycle\Annotated\Annotation\Relation\Inverse;

/**
 * @Entity
 */
class HasOneTestModel extends BaseTestModel
{

    public $has_one_inverse;

    /** @HasMany(target=HasManyTestModel::class, inverse=@Inverse(type="belongsTo", as="has_many_inverse"), nullable=true)
     */
    public $has_many;
}
