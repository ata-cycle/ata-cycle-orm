<?php

namespace Ata\Cycle\ORM\Tests\Models;

use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;

/**
 * @Entity
 */
class TestRelatedModel extends BaseTestModel
{
   public $parent;
}
