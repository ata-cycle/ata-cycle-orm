<?php

namespace Ata\Cycle\ORM\Tests\Models;

use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\{Column, Entity, Relation\HasMany, Relation\Inverse};

/**
 * @Entity
 */
class TestModel extends BaseTestModel
{
    /**
     * @HasMany(target=TestRelatedModel::class, inverse = @Inverse(as = "parent", type = "belongsTo"), nullable=true)
    */
    public $related;
}
