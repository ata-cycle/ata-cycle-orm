<?php

namespace Ata\Cycle\ORM\Console\Commands;

use Ata\Cycle\ORM\Console\Commands\Traits\GetMigrationFileName;
use Illuminate\Console\Command;

class Fresh extends Command
{
    use GetMigrationFileName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'cycle:fresh';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop all tables and run all migrations';

    public function handle()
    {
        $this->call('db:wipe');

        $this->call('cycle:migrate');
    }
}
