<?php


namespace Ata\Cycle\ORM\Mappers\Commands\Delete;


use Ata\Cycle\ORM\Mappers\Commands\Interfaces\DeleteMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Cycle\ORM\Command\CommandInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class Delete implements DeleteMapperCommand
{

    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?CommandInterface $previousCommand): ?CommandInterface
    {
        return $mapper->defaultDelete($entity, $node, $state);
    }
}
