<?php

namespace Ata\Cycle\ORM\Models\Traits;

use Cycle\ORM\Select\ConstrainInterface;
use Illuminate\Support\Collection;

trait RepositoryMethods
{
    /**
     * @inheritDoc
     */
    static function findByPK($id)
    {
        return static::__callStatic('findByPK', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function findOne(array $scope = [])
    {
        return static::__callStatic('findOne', func_get_args());
    }

    static function findAll(array $scope = []): iterable
    {
        return static::__callStatic('findAll', func_get_args());
    }

    static function findOrFail(array $scope = []): Collection
    {
        return static::__callStatic('findOrFail', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function firstOrFail($id = null)
    {
        return static::__callStatic('firstOrFail', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function firstOrCreate(array $requiredData = [], array $creatingData = [])
    {
        return static::__callStatic('firstOrCreate', func_get_args());
    }

    static function updateOrCreate(array $updatingData = [], array $requiredData = []): Collection
    {
        return static::__callStatic('updateOrCreate', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function withoutConstrains()
    {
        return static::__callStatic('withoutConstrains', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function withConstrain(ConstrainInterface $constrain)
    {
        return static::__callStatic('withConstrain', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function distinct()
    {
        return static::__callStatic('distinct', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function where(...$args)
    {
        return static::__callStatic('where', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function whereIf(bool $predicate, ...$args)
    {
        return static::__callStatic('whereIf', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function load($relation, array $options = [])
    {
        return static::__callStatic('load', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function loadLeftJoin($relation, array $options = [])
    {
        return static::__callStatic('loadLeftJoin', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function andWhere(...$args)
    {
        return static::__callStatic('andWhere', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function orWhere(...$args)
    {
        return static::__callStatic('orWhere', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function having(...$args)
    {
        return static::__callStatic('having', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function andHaving(...$args)
    {
        return static::__callStatic('andHaving', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function orHaving(...$args)
    {
        return static::__callStatic('orHaving', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function orderBy($expression, $direction = 'ASC')
    {
        return static::__callStatic('orderBy', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function limit(int $limit)
    {
        return static::__callStatic('limit', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function offset(int $limit)
    {
        return static::__callStatic('offset', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function paginate(int $perPage, int $pageNumber = 0)
    {
        return static::__callStatic('paginate', func_get_args());
    }

    static function exists(): bool
    {
        return static::__callStatic('exists', func_get_args());
    }

    static function notExists(): bool
    {
        return static::__callStatic('notExists', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function avg($identifier)
    {
        return static::__callStatic('avg', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function min($identifier)
    {
        return static::__callStatic('min', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function max($identifier)
    {
        return static::__callStatic('max', func_get_args());
    }

    /**
     * @inheritDoc
     */
    static function sum($identifier)
    {
        return static::__callStatic('sum', func_get_args());
    }

}
