<?php


namespace Ata\Cycle\ORM\Mappers\Commands\Interfaces;


use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Cycle\ORM\Command\CommandInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

interface DeleteMapperCommand
{
    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?CommandInterface $previousCommand): ?CommandInterface;
}
