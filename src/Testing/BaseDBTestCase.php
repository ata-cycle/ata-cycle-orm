<?php


namespace Ata\Cycle\ORM\Testing;

use Ata\Cycle\ORM\Drivers\PostgresDriver;
use Ata\Cycle\ORM\Mappers\Commands\Delete\SoftDelete;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Orchestra\Testbench\TestCase;
use Spiral\Database\DatabaseInterface;
use Ata\Cycle\ORM\Mappers\Commands\Create;
use Ata\Cycle\ORM\Mappers\Commands\Update;
use Jchook\AssertThrows\AssertThrows;

abstract class BaseDBTestCase extends TestCase
{
    use AssertThrows;

    protected abstract function getSourceClass();

    /**
     * @var TestLogger
     */
    private $testLogger;

    protected function getEnvironmentSetUp($app)
    {
        $cycleConfig = include __DIR__ . '/../../config/cycle.php';

        $app['config']->set('cycle', $cycleConfig);
        $app['config']->set('cycle.schema.path', '/app/tests/Models');

        $app['config']->set('cycle.migrations.directory', '/app/tests/cycle_migrations');

        $app['config']->set('cycle.database.databases.default.connection', 'postgres');

        $app['config']->set('cycle.database.connections.postgres.driver', PostgresDriver::class);
        $app['config']->set('cycle.database.connections.postgres.options.connection', 'pgsql:host=pgsql;dbname=pg-data');
        $app['config']->set('cycle.database.connections.postgres.options.username', 'pg-user');
        $app['config']->set('cycle.database.connections.postgres.options.password', 'pg-secret');
        $app['config']->set('cycle.testing.source_class', TestModel::class);

        if ($this->withTimestamps()){
            $app['config']->set('cycle.commands.create', [
                Create\Create::class,
                Create\Timestamps::class,
            ]);

            $app['config']->set('cycle.commands.update', [
                Update\Update::class,
                Update\Timestamps::class,
            ]);

            $app['config']->set('cycle.commands.delete', [
                SoftDelete::class,
            ]);
        }
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('cycle:install');
        $this->artisan('cycle:make-migration');
        $this->artisan('cycle:migrate');

        $this->testLogger = new TestLogger();

        resolve('cycle-db')->getSource(static::getSourceClass())->getDatabase()->getDriver()->setLogger($this->testLogger);

        $this->createEntities();

        resolve('cycle-db.heap-clean');
    }

    protected function enableLog()
    {
        $this->testLogger->display();
    }

    protected function withTimestamps()
    {
        return false;
    }

    protected function disableLog()
    {
        $this->testLogger->hide();
    }

    protected function tearDown(): void
    {
        /** @var DatabaseInterface */
        $db = resolve('cycle-db')->getSource(static::getSourceClass())->getDatabase();

        if ($this->dropTables())
        {
            foreach ($db->getTables() as $table)
            {
                if ($table->getName() !== config('cycle.migrations.table'))
                {
                    $db->delete($table->getName())->run();
                }
            }
        }

        if ($this->shouldDeleteMigrations())
        {
            $this->artisan('cycle:rollback --number=1');

            array_map('unlink', glob('/app/tests/cycle_migrations/*.php'));
        }

        parent::tearDown();
    }

    protected function dropTables(): bool
    {
        return true;
    }

    protected function shouldDeleteMigrations(): bool
    {
        return false;
    }

    protected function createEntities(){}

    protected function getPackageProviders($app)
    {
        return ['Ata\Cycle\ORM\PackageServiceProvider'];
    }
}
