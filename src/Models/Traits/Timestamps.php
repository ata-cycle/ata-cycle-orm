<?php

namespace Ata\Cycle\ORM\Models\Traits;

use Cycle\Annotated\Annotation\Column;
use Ata\Cycle\ORM\Typecasts\Carbon;

trait Timestamps
{
    /**  @Column(type="datetime", typecast=Carbon::class) */
    public $created_at;

    /**  @Column(type="datetime", typecast=Carbon::class) */
    public $updated_at;
}
