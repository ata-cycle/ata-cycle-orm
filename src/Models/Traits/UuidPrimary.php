<?php

namespace Ata\Cycle\ORM\Models\Traits;

use Cycle\Annotated\Annotation\Column;

trait UuidPrimary
{
    /**  @Column(type="string(36)", primary=true) */
    public $id;
}
