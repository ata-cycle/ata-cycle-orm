<?php


namespace Ata\Cycle\ORM\Mappers\Commands\Update;


use Ata\Cycle\ORM\Mappers\Commands\Interfaces\UpdateMapperCommand;
use Ata\Cycle\ORM\Mappers\DefaultMapperInterface;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;

class Update implements UpdateMapperCommand
{
    function run(DefaultMapperInterface $mapper, $entity, Node $node, State $state, ?ContextCarrierInterface $previousCommand): ?ContextCarrierInterface
    {
        return $mapper->defaultUpdate($entity, $node, $state);
    }
}
