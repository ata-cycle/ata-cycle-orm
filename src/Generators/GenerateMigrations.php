<?php


namespace Ata\Cycle\ORM\Generators;


use Cycle\Migrations\GenerateMigrations as CycleGenerateMigrations;
use Cycle\Migrations\MigrationImage;
use Cycle\Schema\Generator\SyncTables;
use Cycle\Schema\Registry;
use Illuminate\Console\OutputStyle;
use Spiral\Database\Schema\AbstractTable;
use Spiral\Database\Schema\Reflector;
use Spiral\Migrations\Atomizer\Atomizer;
use Spiral\Migrations\Atomizer\Renderer;
use Spiral\Migrations\Config\MigrationConfig;
use Spiral\Migrations\RepositoryInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateMigrations extends CycleGenerateMigrations
{
    // Migrations file name format. This format will be used when requesting new migration filename.
    private const FILENAME_FORMAT = '%s_%04d_%s';

    /**
     * @var string
     */
    private $defaultImageName;
    /**
     * @var OutputInterface
     */
    private $output;
    /**
     * @var RepositoryInterface
     */
    private $migrationRepository;
    /**
     * @var bool
     */
    private $empty;
    /**
     * @var MigrationConfig
     */
    private $migrationConfig;

    public function __construct(
        RepositoryInterface $migrationRepository,
        MigrationConfig $migrationConfig,
        ?string $name,
        OutputStyle $output,
        $empty = false
    )
    {
        $this->defaultImageName = $name ?? config('cycle.migrations.default_migration_name');
        $this->output = $output;

        parent::__construct($migrationRepository, $migrationConfig);
        $this->migrationRepository = $migrationRepository;
        $this->empty = $empty;
        $this->migrationConfig = $migrationConfig;
    }

    /**
     * @param Registry $registry
     * @return Registry
     */
    public function run(Registry $registry): Registry
    {
        $databases = [];
        foreach ($registry as $e) {
            if ($registry->hasTable($e) && !$e->getOptions()->has(SyncTables::READONLY_SCHEMA)) {
                $databases[$registry->getDatabase($e)][] = $registry->getTableSchema($e);
            }
        }

        foreach ($databases as $database => $tables) {
            $reflector = new Reflector();
            foreach ($tables as $table) {
                $reflector->addTable($table);
            }

            $sorted = $reflector->sortedTables();
            foreach ($sorted as $table) {
                $image = $this->generate($database, [$table]);
                if (!$image) {
                    // no changes
                    continue;
                }
                $class = $image->getClass()->getName();
                $name = substr($image->buildFileName(), 0, 128);

                $this->migrationRepository->registerMigration($name, $class, $image->getFile()->render());
            }
        }

        return $registry;
    }


    protected function generate(string $database, array $tables): ?MigrationImage
    {
        $result = $this->empty ? $this->generateEmptyMigration($database) : parent::generate($database, $tables);

        if ($result) {
            $result->setName(sprintf(self::FILENAME_FORMAT,
                date(config('cycle.migrations.timestamp_format')),
                $this->getChunk(),
                $result->getName()));
            $result->fileNamePattern = '{name}';
            $this->output->writeln('');
            $this->output->writeln('Generate migration ' . $result->buildFileName());
        }

        return $result;
    }

    private function generateEmptyMigration(string $database)
    {
        $atomizer = new Atomizer(new Renderer());

        $image = new MigrationImage($this->migrationConfig, $database);

        $atomizer->declareChanges($image->getClass()->method('up')->getSource());
        $atomizer->revertChanges($image->getClass()->method('down')->getSource());

        return $image;
    }

    protected function getChunk()
    {
        $lastMigrationName = collect($this->migrationRepository->getMigrations())->keys()->last();

        if (is_null($lastMigrationName)) {
            return 1;
        }

        $definition = explode('_', basename($lastMigrationName));

        return ((int)$definition[1]) + 1;
    }


    /**
     * @param Atomizer $atomizer
     * @return string
     */
    private function generateName(Atomizer $atomizer): string
    {
        $name = [];

        foreach ($atomizer->getTables() as $table) {
            if ($table->getStatus() === AbstractTable::STATUS_NEW) {
                $name[] = 'create_' . $table->getName();
                continue;
            }

            if ($table->getStatus() === AbstractTable::STATUS_DECLARED_DROPPED) {
                $name[] = 'drop_' . $table->getName();
                continue;
            }

            if ($table->getComparator()->isRenamed()) {
                $name[] = 'rename_' . $table->getInitialName();
                continue;
            }

            $name[] = 'change_' . $table->getName();

            $comparator = $table->getComparator();

            foreach ($comparator->addedColumns() as $column) {
                $name[] = 'add_' . $column->getName();
            }

            foreach ($comparator->droppedColumns() as $column) {
                $name[] = 'rm_' . $column->getName();
            }

            foreach ($comparator->alteredColumns() as $column) {
                $name[] = 'alter_' . $column[0]->getName();
            }

            foreach ($comparator->addedIndexes() as $index) {
                $name[] = 'add_index_' . $index->getName();
            }

            foreach ($comparator->droppedIndexes() as $index) {
                $name[] = 'rm_index_' . $index->getName();
            }

            foreach ($comparator->alteredIndexes() as $index) {
                $name[] = 'alter_index_' . $index[0]->getName();
            }

            foreach ($comparator->addedForeignKeys() as $fk) {
                $name[] = 'add_fk_' . $fk->getName();
            }

            foreach ($comparator->droppedForeignKeys() as $fk) {
                $name[] = 'rm_fk_' . $fk->getName();
            }

            foreach ($comparator->alteredForeignKeys() as $fk) {
                $name[] = 'alter_fk_' . $fk[0]->getName();
            }
        }

        return implode('_', $name);
    }
}
