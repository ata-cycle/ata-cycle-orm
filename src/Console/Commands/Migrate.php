<?php

namespace Ata\Cycle\ORM\Console\Commands;

use Ata\Cycle\ORM\Console\Commands\Traits\GetMigrationFileName;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Spiral\Migrations\Migrator;
use Spiral\Migrations\State;
use Symfony\Component\Console\Input\InputOption;

class Migrate extends Command
{
    use GetMigrationFileName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'cycle:migrate';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migrations from the configured directory';
    /**
     * @var Migrator
     */
    private $migrator;

    /**
     * Create a new command instance.
     * @param Migrator $migrator
     */
    public function __construct(Migrator $migrator)
    {
        parent::__construct();
        $this->migrator = $migrator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Throwable
     */
    public function handle()
    {
        $name = $this->input->getOption("name");
        $number = (int)$this->input->getOption("number");
        $timestamp = $this->input->getOption("timestamp");
        $step = (int)$this->input->getOption("step");

        if ($this->input->getOption('wipe')){
            $this->call('db:wipe');
        }

        if (!$this->migrator->isConfigured()){
            $this->migrator->configure();
        }

        $hasPendingMigrations = collect($this->migrator->getMigrations())->filter(function($migration){
            return $migration->getState()->getStatus() === State::STATUS_PENDING;
        })->isNotEmpty();

        if (!$hasPendingMigrations){
            $this->warn('Nothing to migrate');
            return;
        }

        foreach ($this->migrator->getMigrations() as $migrating) {
            if ($migrating->getState()->getStatus() !== State::STATUS_PENDING) {
                continue;
            }
            $this->line('<comment>Migrating:</comment> ' . $this->getMigrationFileName($migrating));
            $migratied = $this->migrator->run();
            $this->line('<info>Migrated:</info>  ' . $this->getMigrationFileName($migratied));

            if ($step > 0) {
                if (--$step > 0) {
                    continue;
                }

                return;
            }

            // Если передан таймштамп
            if (!is_null($timestamp)) {

                if ($migratied->getState()->getTimeCreated() >= \DateTime::createFromFormat(
                        config('cycle.migrations.timestamp_format'),
                        $timestamp
                    )) {
                    return;
                }

                continue;
            }

            // Если передан  номер миграции
            if ($number > 0) {
                if ((int)explode('_', $migratied->getState()->getName())[0] >= $number) {
                    return;
                }
                continue;
            }

            // Если передано имя миграции
            if (!is_null($name)) {
                if (Str::contains($migratied->getState()->getName(), $name)) {
                    return;
                }
                continue;
            }

            // Если не указали имя, номер миграции и таймстамп, значит, накатываем все миграции до конца
        }

        $this->info('Database migrating completed successfully.');

        if ($this->input->getOption('seed')){
            $this->call('db:seed');
        }

        return;
    }

    protected function getOptions()
    {
        return [
            ['name', null, InputOption::VALUE_OPTIONAL, 'default value "auto"'],
            ['number', 'N', InputOption::VALUE_OPTIONAL, 'number of migration'],
            ['timestamp', 'ts', InputOption::VALUE_OPTIONAL, 'migration timestamp'],
            ['step', 's', InputOption::VALUE_OPTIONAL, 'migration step count'],
            ['seed', null, InputOption::VALUE_NONE, 'run seeds after migrate'],
            ['wipe', null, InputOption::VALUE_NONE, 'wipe database before migrations'],
        ];
    }
}
