<?php


namespace Ata\Cycle\ORM\Console\Commands;


use Ata\Cycle\ORM\Console\Commands\Traits\GetMigrationFileName;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Spiral\Migrations\Migrator;
use Spiral\Migrations\State;
use Symfony\Component\Console\Input\InputOption;

class Rollback extends Command
{
    use GetMigrationFileName;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'cycle:rollback';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rollback migration';
    /**
     * @var Migrator
     */
    private $migrator;

    /**
     * Create a new command instance.
     * @param Migrator $migrator
     */
    public function __construct(Migrator $migrator)
    {
        parent::__construct();
        $this->migrator = $migrator;
    }

    public function handle()
    {
        if (!$this->migrator->isConfigured())
        {
            $this->migrator->configure();
        }

        $name = $this->input->getOption("name");
        $number = (int)$this->input->getOption("number");
        $timestamp = $this->input->getOption("timestamp");
        $step = (int)$this->input->getOption("step");

        $hasPendingMigrations = collect($this->migrator->getMigrations())->filter(function($migration){
            return $migration->getState()->getStatus() === State::STATUS_EXECUTED;
        })->isNotEmpty();

        if (!$hasPendingMigrations){
            $this->warn('Nothing to rollback');
            return;
        }

        while (($migration = $this->migrator->rollback()) != null) {
            $this->warn('Migration ' . $this->getMigrationFileName($migration) . ' successfully rollbacked');

            if ($step > 0 && --$step > 0) {
                continue;
            }

            // Если передан таймштамп
            if (!is_null($timestamp)) {

                if ($migration->getState()->getTimeCreated() <= \DateTime::createFromFormat(
                        config('cycle.migrations.timestamp_format'),
                        $timestamp
                    )) {
                    return;
                }

                continue;
            }

            // Если передан  номер миграции
            if ($number > 0) {
                if ((int)explode('_', $migration->getState()->getName())[0] <= $number) {
                    return;
                }
                continue;
            }

            // Если передано имя миграции
            if (!is_null($name)) {
                if (Str::contains($migration->getState()->getName(), $name)) {
                    return;
                }
                continue;
            }

            // Если не указали имя, номер миграции и таймстамп, значит, откатываем на одну миграцию
            return;
        }
    }

    protected function getOptions()
    {
        return [
            ['name', null, InputOption::VALUE_OPTIONAL, 'default value "auto"'],
            ['number', null, InputOption::VALUE_OPTIONAL, 'number of migration'],
            ['timestamp', 'ts', InputOption::VALUE_OPTIONAL, 'migration timestamp'],
            ['step', 's', InputOption::VALUE_OPTIONAL, 'migration step count'],
        ];
    }
}
