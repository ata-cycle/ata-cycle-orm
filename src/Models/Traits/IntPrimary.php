<?php


namespace Ata\Cycle\ORM\Models\Traits;


use Cycle\Annotated\Annotation\Column;

trait IntPrimary
{
    /**  @Column(type="primary") */
    public $id;
}
