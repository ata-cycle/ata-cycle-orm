<?php

namespace Ata\Cycle\ORM\Tests\Unit\ModelMethodsTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;
use Illuminate\Support\Collection;

class UpdateOrCreateModelMethodTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    public $model1;
    public $model2;
    public $model3;
    public $idsArray;

    protected function createEntities(): void
    {
        $this->model1 = TestModel::create(['integer_field' => 333]);
        $this->model2 = TestModel::create(['integer_field' => 333]);
        $this->model3 = TestModel::create(['integer_field' => 999]);
        $this->idsArray = [
            $this->model1->id,
            $this->model2->id,
        ];
    }

    /**
     * @throws \Throwable
     */
    public function testUpdateOrCreateShouldUpdateModelTest()
    {
        $neededModels = collect(TestModel::findAll(['integer_field' => 333]));
        resolve('cycle-db.heap-clean');

        $updatedModels = TestModel::where(['integer_field' => 333])->updateOrCreate(['integer_field' => 444]);
        resolve('cycle-db.heap-clean');

        $this->assertInstanceOf(Collection::class, $updatedModels);
        $this->assertCount($neededModels->count(), $updatedModels);
        $updatedModels->each(function ($element) {
            $this->assertEquals(444, $element->integer_field);
        });
    }

    public function testUpdateOrCreateShouldCreateModelTest()
    {
        $createdModel = TestModel::where(['integer_field' => 111])->updateOrCreate(
            ['integer_field' => 222], ['string_field' => 'qwerty']
        );

        $this->assertInstanceOf(Collection::class, $createdModel);
        $createdModel->each(function ($element) {
            $this->assertFalse(in_array($element->id, $this->idsArray));
            $this->assertEquals(222, $element->integer_field);
            $this->assertEquals('qwerty', $element->string_field);
        });

        resolve('cycle-db.heap-clean');
        $this->assertCount(0, collect(TestModel::findAll(['integer_field' => 111])));
        $this->assertCount(1, collect(TestModel::findAll(['integer_field' => 222])));
    }
}
