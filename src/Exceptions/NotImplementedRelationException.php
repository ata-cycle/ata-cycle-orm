<?php

namespace Ata\Cycle\ORM\Exceptions;

use BadMethodCallException;
use Cycle\ORM\Relation;

class NotImplementedRelationException extends BadMethodCallException
{
    public const RELATION_TYPES = [
        Relation::EMBEDDED => 'Embedded',
        Relation::HAS_ONE => 'Has One',
        Relation::HAS_MANY => 'Has Many',
        Relation::BELONGS_TO => 'Belongs To',
        Relation::REFERS_TO => 'Refers To',
        Relation::MANY_TO_MANY => 'Many To Many',
        Relation::BELONGS_TO_MORPHED => 'Belongs To Morphed',
        Relation::MORPHED_HAS_ONE => 'Morphed Has One',
        Relation::MORPHED_HAS_MANY => 'Morphed Has Many',
    ];

    public function __construct(
        $modelClass, $relationName, $relationType
    ) {
        $relationType = self::RELATION_TYPES[$relationType];

        parent::__construct("Relation $relationName with type $relationType in class $modelClass not implemented for updating relations");
    }
}
