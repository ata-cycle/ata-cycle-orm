<?php

namespace Ata\Cycle\ORM\Tests\Unit\ModelMethodsTests;

use Ata\Cycle\ORM\Testing\BaseDBTestCase;
use Ata\Cycle\ORM\Tests\Models\TestModel;

class WhereIfModelMethodTest extends BaseDBTestCase
{
    protected function getSourceClass()
    {
        return TestModel::class;
    }

    protected function createEntities()
    {
        TestModel::create(['integer_field' => 1]);
        TestModel::create(['integer_field' => 2]);
    }

    public function testShouldAddWhereIfPredicateIsTrue()
    {
        $result = TestModel::whereIf(true, 'integer_field', 2)->orderBy('integer_field')->findOne();

        self::assertEquals(2, $result->integer_field);
    }

    public function testShouldNotAddWhereIfPredicateIsFalse()
    {
        $result = TestModel::whereIf(false, 'integer_field', 2)->orderBy('integer_field')->findOne();

        self::assertEquals(1, $result->integer_field);
    }
}
