<?php

namespace Ata\Cycle\ORM\Tests\Models\RelatedModels;

use Ata\Cycle\ORM\Testing\BaseTestModel;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\ManyToMany;

/**
 * @Entity
 */
class HasManyTestModel extends BaseTestModel
{
    /**
     * @ManyToMany(target=ManyToManyTestModel::class, though=PivotTestModel::class, cascade=true)
     */
    public $many_to_many;

    public $has_many_inverse;
}
